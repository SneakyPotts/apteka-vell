<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->IncludeComponent(
    "bitrix:sale.basket.basket.line",
    ".default",
    array(
        "HIDE_ON_BASKET_PAGES" => "N",
        "PATH_TO_AUTHORIZE" => "",
        "PATH_TO_BASKET" => "/cart/",
        "PATH_TO_ORDER" => "/order/",
        "PATH_TO_PERSONAL" => SITE_DIR."/personal/",
        "PATH_TO_PROFILE" => SITE_DIR."/personal/",
        "PATH_TO_REGISTER" => SITE_DIR."/login/",
        "POSITION_FIXED" => "N",
        "SHOW_AUTHOR" => "N",
        "SHOW_EMPTY_VALUES" => "Y",
        "SHOW_NUM_PRODUCTS" => "Y",
        "SHOW_PERSONAL_LINK" => "N",
        "SHOW_PRODUCTS" => "N",
        "SHOW_REGISTRATION" => "N",
        "SHOW_TOTAL_PRICE" => "Y",
        "COMPONENT_TEMPLATE" => ".default"
    ),
    false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

