<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Информация");
?><h4>Здесь вы можете получить информацию о нашей компании,
    ознакомиться с официальными документами аптеки, политикой конфиденциальности и т.д.</h4>

    <style>
        .left_sub__menu > ul{
            display: flex;
            flex-direction: column;
            position: relative;
        }
    </style>


<div class="left_sub__menu">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"new_multilevel", 
	array(
		"COMPONENT_TEMPLATE" => "new_multilevel",
		"ROOT_MENU_TYPE" => "left",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>