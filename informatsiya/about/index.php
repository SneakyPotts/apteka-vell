<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?><h1>Интернет-аптека Аптека ВТ</h1>
    <p>
        Интернет-аптека Аптека ВТ&nbsp;- это высочайшие стандарты качества. Теперь не нужно тратить свое время на долгий поиск необходимых лекарств в аптеках!&nbsp;Широчайший&nbsp;ассортимент товаров Вы можете найти в удобной интернет-аптеке Аптека ВТ и заказать его&nbsp;быстро и недорого. В нашем ассорименте лекарственные препараты, травы, биологически-активные добавки, лечебная косметика, товары для мам и малышей, витамины, лекарства и оборудование для реабилитации, препараты для мужского здоровья.
    </p>
    <h3>
        Надежность. Качество. Комфорт.
    </h3>
    <p>
        <strong>Вы цените свое время и деньги?</strong>
    </p>
    <p>
        Вам не нужно стоять в очередях,&nbsp;слышать что лекарства нет в наличии или спешно читать описание препарата и инструкцию.
    </p>
    <p>
        <strong>Вы ищите редкое лекарство?&nbsp;</strong>
    </p>
    <p>
        В нашей аптеке Вы сможете найти даже самые трудные для нахождения препараты. Наш широкий ассортимент и удобный в использовании интернет-магазин поможет Вам быстро и комфортно приобрести&nbsp;все необходимое Вам лекарства.&nbsp;
    </p>
    <p>
        <strong>У Вас имеются вопросы?</strong>
    </p>
    <p>
        Наши опытные фармацевты ответят на все Ваши вопросы и помогут оперативно выбрать максимально эффективное и подходящее именно Вам лекарственное средство.
    </p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>