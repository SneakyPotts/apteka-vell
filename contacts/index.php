<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Найти нас очень просто и мы всегда готовы обслужить желающих приобрести у нас онкопрепараты или получить консультацию. Ждем вас без выходных по указанному адресу, звоните или пишите письма на имейл. Бдуем рады всем!");
$APPLICATION->SetPageProperty("title", "Контакты аптеки \"ВЕЛЛ\" в Москве. Как нас найти? Схема проезда, адрес, телефон");
$APPLICATION->SetTitle("Контакты");
?><h1>Контактная информация онкологической аптеки "ВЕЛЛ"</h1>
<p>
 <b>Адрес:</b>&nbsp;г. Москва, ул. Маршала Василевского, 13 корпус 1
</p>
<p>
 <b>Телефоны:</b>&nbsp;+7 (495) 003-13-03, +7 (499) 193-56-36
</p>
<p>
 <b>E-mail:</b>&nbsp;<a href="mailto:apteka.vell@mail.ru">apteka.vell@mail.ru</a>
</p>
<p>
 <b>Режим работы</b>:&nbsp;Пн - Вс: с 9:00 до 21:00
</p>
<p>
 <br>
</p>
<p>
 <b>Схема проезда:&nbsp;</b>
</p>
<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=bFJMi_2_V3_0QOImO-c4BpQLhNempQNk&amp;height=480&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>