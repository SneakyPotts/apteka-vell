<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");?>

	<div class="bx-404-container text-center">
		<div class="bx-404-block"><img src="<?=SITE_DIR?>images/404.jpg" alt="404"></div>
		<div class="bx-404-text-block" style="line-height: 2;">Неправильно набран адрес, <br>или такой страницы на сайте больше не
            существует.</div>
		<div class="">Вернитесь на <a href="<?=SITE_DIR?>" style="font-weight: bold">главную</a> или воспользуйтесь
            <a href="<?=SITE_DIR?>lekarstvennye-sredstva/" style="font-weight: bold">каталогом</a> сайта
            .</div>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>