<?
$MESS['SETTINGS_BUTTON'] = "Button to close the window";
$MESS['SETTINGS_TEXT'] = "Text";
$MESS['SETTINGS_BUTTON_TEXT'] = "Button text";
$MESS['SETTINGS_BACKGRAUND_COLOR'] = "Background color";
$MESS['SETTINGS_TEXT_COLOR'] = "Text color";
$MESS['SETTINGS_BUTTON_BACKGRAUND_COLOR'] = "The background color of the button";
$MESS['SETTINGS_BUTTON_TEXT_COLOR'] = "The color of the text on the button";
$MESS['SETTINGS_BACKGRAUND_opacity'] = "Transparency";
$MESS['SETTINGS_UUID_CCOOKIE_JS'] = "Enable cookies. To close the window";


$MESS['TEST_TEXT'] = 'The site uses cookies and other similar tools. If you stay on the site after reading this information, it means that you do not object to their use.';
$MESS['TEST_BUTTON_TEXT'] = 'Read';

?>
