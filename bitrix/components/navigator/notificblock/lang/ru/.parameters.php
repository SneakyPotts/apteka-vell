<?
$MESS['SETTINGS_BUTTON'] = "Кнопка для закрытия окна";
$MESS['SETTINGS_TEXT'] = "Текст";
$MESS['SETTINGS_BUTTON_TEXT'] = "Текст кнопки";
$MESS['SETTINGS_BACKGRAUND_COLOR'] = "Цвет фона";
$MESS['SETTINGS_TEXT_COLOR'] = "Цвет текста";
$MESS['SETTINGS_BUTTON_BACKGRAUND_COLOR'] = "Цвет фона кнопки";
$MESS['SETTINGS_BUTTON_TEXT_COLOR'] = "Цвет текста на кнопке";
$MESS['SETTINGS_BACKGRAUND_opacity'] = "Прозрачность [1 до 0.1]";
$MESS['SETTINGS_UUID_CCOOKIE_JS'] = "Включить куки";

$MESS['TEST_TEXT'] = 'На сайте используются файлы cookie и другие аналогичные средства. Если вы остаётесь на сайте после прочтения данной информации, это означает, что вы не возражаете против их использования.<br><a style="float: left;padding: 5px;" href="#f">Политика использования файлов cookie</a>';
$MESS['TEST_BUTTON_TEXT'] = 'Прочитал';
$MESS['SETTINGS'] = 'НАСТРОЙКИ';

$MESS['Y'] = 'Да';
$MESS['N'] = 'Нет';


$MESS['jQuery_on'] = 'Подключить jQuery';
$MESS['m_type'] = 'Вид вывода';

$MESS['m_top'] = 'Вверху';
$MESS['m_bottom'] = 'Внизу';
$MESS['m_left'] = 'Внизу слева';
$MESS['m_right'] = 'Внизу справа';
?>
