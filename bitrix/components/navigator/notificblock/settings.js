
var OnTextAreaConstruct = function(arParams) {
    let _this = this;
    let iInputID   = arParams.oInput.id;
    let iTextAreaID   = iInputID + '_text';
    let divs = document.querySelector('[name="SETTINGS_TEXT"]');
    let completeAction = function(){
      document.getElementById(iInputID).value = BX.proxy_context.value;
      document.getElementById(iTextAreaID).value = BX.proxy_context.value;
    }
    let obLabel   = arParams.oCont.appendChild(BX.create('textarea', {
       props : {
          'cols' : 60,
          'rows' : 6,
          'id' : iTextAreaID,
          'name':arParams.oInput.name,
       },
       events: {
         keyup: BX.proxy(completeAction, _this)
       },
       html: arParams.oInput.value
    }));
}