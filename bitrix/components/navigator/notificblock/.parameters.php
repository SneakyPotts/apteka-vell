<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(CModule::IncludeModule('navigator.notificblock')){
	$arUUID = NotificBlock::getUUID();
}
$arComponentParameters = Array(
	"GROUPS" => array(
		"SETTINGS" => array(
			"NAME" => GetMessage("SETTINGS"),
		),
	),
	"PARAMETERS" => array(
		"jQuery_on" => array(
			"NAME" => GetMessage("jQuery_on"),
			"TYPE" => "CHECKBOX",
			"MULTIPLE" => "N",
			"DEFAULT" => "Y",
			"PARENT" => "DATA_SOURCE",
		),
		"SETTINGS_BUTTON" => array(
			"NAME" => GetMessage("SETTINGS_BUTTON"),
			"TYPE" => "CHECKBOX",
			"MULTIPLE" => "N",
			"DEFAULT" => "Y",
			"PARENT" => "DATA_SOURCE",
		),
		"SETTINGS_TYPE" => array(
			"NAME" => GetMessage("m_type"),
			"TYPE" => "LIST",
			"MULTIPLE" => "N",
			"VALUES" => [
				'top' => GetMessage("m_top"),
				'bottom' => GetMessage("m_bottom"),
				'left' => GetMessage("m_left"),
				'right' => GetMessage("m_right"),
			],
			"PARENT" => "DATA_SOURCE",
		),
		"SETTINGS_TEXT" => array(
			"NAME" => GetMessage("SETTINGS_TEXT"),
			"TYPE"  => "CUSTOM",
			"MULTIPLE" => "N",
			"DEFAULT" => GetMessage("TEST_TEXT"),
			"JS_FILE"   => "/bitrix/components/navigator/notificblock/settings.js",
			"JS_EVENT"   => "OnTextAreaConstruct",
			"PARENT" => "SETTINGS",
		),
		"SETTINGS_BUTTON_TEXT" => array(
			"NAME" => GetMessage("SETTINGS_BUTTON_TEXT"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => GetMessage("TEST_BUTTON_TEXT"),
			"COLS" => 25,
			"PARENT" => "SETTINGS",
		),
		"SETTINGS_BACKGRAUND_COLOR" => array(
			"NAME" => GetMessage("SETTINGS_BACKGRAUND_COLOR"),
			"TYPE" => "COLORPICKER",
			"DEFAULT" => "#333",
			"COLS" => 25,
			"PARENT" => "SETTINGS",
		),
		"SETTINGS_TEXT_COLOR" => array(
			"NAME" => GetMessage("SETTINGS_TEXT_COLOR"),
			"TYPE" => "COLORPICKER",
			"DEFAULT" => "#fff",
			"COLS" => 25,
			"PARENT" => "SETTINGS",
		),
		"SETTINGS_BUTTON_BACKGRAUND_COLOR" => array(
			"NAME" => GetMessage("SETTINGS_BUTTON_BACKGRAUND_COLOR"),
			"TYPE" => "COLORPICKER",
			"DEFAULT" => "#ff3366",
			"COLS" => 25,
			"PARENT" => "SETTINGS",
		),
		"SETTINGS_BUTTON_TEXT_COLOR" => array(
			"NAME" => GetMessage("SETTINGS_BUTTON_TEXT_COLOR"),
			"TYPE" => "COLORPICKER",
			"DEFAULT" => "#fff",
			"COLS" => 25,
			"PARENT" => "SETTINGS",
		),
		"SETTINGS_BACKGRAUND_opacity" => array(
			"NAME" => GetMessage("SETTINGS_BACKGRAUND_opacity"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "0.9",
			"COLS" => 25,
			"PARENT" => "SETTINGS",
		),
		"SETTINGS_UUID_CCOOKIE_JS" => Array(
			"NAME" => GetMessage("SETTINGS_UUID_CCOOKIE_JS"),
			"TYPE" => "LIST",
			"MULTIPLE" => "N",
			"VALUES" => [
				$arUUID => GetMessage("Y"),
				'' => GetMessage("N"),
			],
			"PARENT" => "DATA_SOURCE",
		),
	)
);
?>