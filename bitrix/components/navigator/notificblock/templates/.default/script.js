(function() {
    notificblock = function(param){
        this.param = param;

    }
    notificblock.prototype.init = function(){
            if(this.setblock()){
                this.setblockNotific();
            }
           
    }
    notificblock.prototype.setblock = function () {
        if(document.querySelector('body') && !document.querySelector('#navNotificBlock.'+this.param.TYPE)){
            var node = BX.create('div', {
                attrs: {
                    className: 'navNotificBlock '+this.param.TYPE,
                    id: 'navNotificBlock',
                    'data-moduleid': 'navigator.notificblock',
                 },
            });
            BX.append(node, document.querySelector('body'));
        }
        return true;
    }
    notificblock.prototype.setblockNotific = function () {
        var ids = this.param.UUID,
        cuuid = this.param.UUID_CCOOKIE,
        blockid = 'nav-js-notific-'+ids;
        //this.blockid = blockid;

        
        var node_div = BX.create('div', {
            attrs: {
                className: 'nav-block-notific active',
                style:'background:'+this.param.BACKGRAUND_COLOR+'; color:'+this.param.TEXT_COLOR+'; opacity: '+this.param.BACKGRAUND_opacity+';',
                id: blockid,
             },
             html: '<span class="body">'+this.param.TEXT+'</span><div class="JS-BUTTON"></div>',
        });
        

        var node_button = BX.create('button', {
            attrs: {
                className: 'nav-block-notific-button',
                style:'background:'+this.param.BUTTON_BACKGRAUND_COLOR+'; color:'+this.param.BUTTON_TEXT_COLOR+';',
             },
             events:{
                click: function(){
                    CCookie.set(cuuid,'Y');
                    document.querySelector('#'+blockid).remove();
                }
             },
             text: this.param.BUTTON_TEXT,
        });

        if(CCookie.get(cuuid) != 'Y'){
            BX.append(node_div, document.querySelector('#navNotificBlock.'+this.param.TYPE));
            if(this.param.SETTINGS_BUTTON == 'Y'){
                BX.append(node_button, document.querySelector('#'+blockid+' .JS-BUTTON'));
            }
           
        }

    }
})()

 