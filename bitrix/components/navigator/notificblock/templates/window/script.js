(function() {
    windownotificblock = function(param){
        this.param = param;

    }
    windownotificblock.prototype.init = function(){
            if(this.setblock()){
                
            }   
    }
    windownotificblock.prototype.setblock = function () {
        if(document.querySelector('.NavBxJsNotific'+this.param.UUID)){
            var windowelement = document.querySelector('.NavBxJsNotific'+this.param.UUID);
            var node = BX.create('div', {
                attrs: {
                    className: 'navNotificBlock window',
                    id: 'navNotificBlock'+this.param.UUID,
                    'data-moduleid': 'navigator.notificblock',
                 },
            });
            BX.append(node, windowelement);
            this.setblockNotific();
        }  return true;
        
    }
    windownotificblock.prototype.setblockNotific = function () {
        var ids = this.param.UUID,
        cuuid = this.param.UUID_CCOOKIE,
        blockid = 'nav-js-notific-'+ids;
        //this.blockid = blockid;

        
        var node_div = BX.create('div', {
            attrs: {
                className: 'nav-block-notific active',
                style:'background:'+this.param.BACKGRAUND_COLOR+'; color:'+this.param.TEXT_COLOR+'; opacity: '+this.param.BACKGRAUND_opacity+';',
                id: blockid,
             },
             html: '<span class="body">'+this.param.TEXT+'</span><div class="JS-BUTTON"></div>',
        });
        

        var node_button = BX.create('button', {
            attrs: {
                className: 'nav-block-notific-button',
                style:'background:'+this.param.BUTTON_BACKGRAUND_COLOR+'; color:'+this.param.BUTTON_TEXT_COLOR+';',
             },
             events:{
                click: function(){
                    CCookie.set(cuuid,'Y');
                    document.querySelector('#'+blockid).remove();
                }
             },
             text: this.param.BUTTON_TEXT,
        });

        if(CCookie.get(cuuid) != 'Y'){
            console.log(document.querySelector('#navNotificBlock'+this.param.UUID));
            BX.append(node_div, document.querySelector('#navNotificBlock'+this.param.UUID));
            if(this.param.SETTINGS_BUTTON == 'Y'){
                BX.append(node_button, document.querySelector('#'+blockid+' .JS-BUTTON'));
            }
           
        }
        return true;
    }
})()