<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?global $APPLICATION;
$APPLICATION->AddHeadScript("/bitrix/components/navigator/notificblock/ccookie.js" );?>
<div class='NavBxJsNotific<?=$arResult['UUID']?>'></div>
<script data-moduleid="navigator.notificblock" data-version="1.0.0">
var notificblock_<?=$arResult['UUID']?> = new windownotificblock({
	UUID: '<?=$arResult['UUID']?>',
	UUID_CCOOKIE: '<?=$arParams['SETTINGS_UUID_CCOOKIE_JS']?>',
	SETTINGS_BUTTON: '<?=$arParams['SETTINGS_BUTTON']?>',
	TYPE:'<?=$arParams['SETTINGS_TYPE']?$arParams['SETTINGS_TYPE']:'top'?>',
	TEXT:<?=CUtil::PhpToJSObject(htmlspecialchars_decode($arParams['SETTINGS_TEXT']))?>,
	BUTTON_TEXT:'<?=$arParams['SETTINGS_BUTTON_TEXT']?>',
	BACKGRAUND_opacity:'<?=$arParams['SETTINGS_BACKGRAUND_opacity']?$arParams['SETTINGS_BACKGRAUND_opacity']:'0.9'?>',
	BACKGRAUND_COLOR:'<?=$arParams['SETTINGS_BACKGRAUND_COLOR']?$arParams['SETTINGS_BACKGRAUND_COLOR']:'#000'?>',
	TEXT_COLOR:'<?=$arParams['SETTINGS_TEXT_COLOR']?$arParams['SETTINGS_TEXT_COLOR']:'#fff'?>',
	BUTTON_BACKGRAUND_COLOR:'<?=$arParams['SETTINGS_BUTTON_BACKGRAUND_COLOR']?$arParams['SETTINGS_BUTTON_BACKGRAUND_COLOR']:'#ff3366'?>',
	BUTTON_TEXT_COLOR:'<?=$arParams['SETTINGS_BUTTON_TEXT_COLOR']?$arParams['SETTINGS_BUTTON_TEXT_COLOR']:'#fff'?>',
});
notificblock_<?=$arResult['UUID']?>.init();
</script>