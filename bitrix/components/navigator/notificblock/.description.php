<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("NAV_notificblock_DEFAULT_TEMPLATE_NAME"),
	"DESCRIPTION" => GetMessage("NAV_notificblock_DEFAULT_TEMPLATE_DESCRIPTION"),
	"SORT" => 100,
	"PATH" => array(
		"ID" => "Navigator",
		/*"CHILD" => array(
			"ID" => "notificblock",
			"NAME" => GetMessage("NAV_notificblock")
		)*/
	),
);
?>