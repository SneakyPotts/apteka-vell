<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Config\Option;

global $APPLICATION;

if(CModule::IncludeModule('navigator.notificblock')){
	$arResult['UUID'] = NotificBlock::getUUID();
	$this->IncludeComponentTemplate();
}


?>