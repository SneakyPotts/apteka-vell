<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

        <ul>
			<? foreach ($arResult as $menuList) { ?>
                <li>
                    <a href="<?= $menuList["LINK"] ?>"><?= $menuList["TEXT"] ?></a>
                </li>
			<? } ?>
        </ul>

<?endif?>
