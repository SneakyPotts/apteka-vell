<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>

<? if (!empty($arResult)): ?>
    <ul class="sub_menu__list">
		<? foreach ($arResult as $menuList) { ?>
            <li class="sub_menu__item">
                <a class="sub_menu__link" href="<?= $menuList["LINK"] ?>"><?= $menuList["TEXT"] ?></a>
            </li>
			<? } ?>
    </ul>

<? endif ?>