<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="newssect">
<div class="news-blocks">
	<div class="news-body">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			//echo "<pre>";
			//print_r($arItem);
			?>
			<div class="newsblock flleft">
		        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><div class="img_news" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></div></a>
		        <h3><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h3>
		        <p>
		         <?=$arItem["PREVIEW_TEXT"]?>
		        </p>
		        <span><?=$arItem["ACTIVE_FROM"]?></span>
		      </div> 
		<?endforeach;?>
	</div>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
</div>
<div style="clear:both;"></div>