<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";
//print_r($arResult);

foreach($arResult["STORES"] as $pid => $arProperty):?>
	<?if($arProperty["REAL_AMOUNT"]>0){?>
          <li> <span> <span itemprop="availability" class="count esct">Товар есть в наличии (<?=$arProperty["REAL_AMOUNT"]?> шт.)</span> </span></li>
    <?} else {?>    
            <li> <span> <span itemprop="availability" class="count no">Товара временно нет на складе! Доступно под заказ</span> </span></li>         
     <?}?> 
<?endforeach;?>
	