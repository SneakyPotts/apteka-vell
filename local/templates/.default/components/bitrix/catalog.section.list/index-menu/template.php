<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?foreach ($arResult['SECTIONS'] as &$arSection) {?>
<a class="item" href="<?=$arSection['SECTION_PAGE_URL']?>">
	<span class="item-img__wrapper">
		<?if($arSection['PICTURE']['SRC']!="") {?>
	    	<img alt="<?=$arSection["NAME"]?>" title="<?=$arSection["NAME"]?>" src="<?=$arSection['PICTURE']['SRC']?>">
	    <?} else {?>
	    	<img alt="<?=$arSection["NAME"]?>" title="<?=$arSection["NAME"]?>" src="/local/templates/index/images/no_photo.png">	
    <?}?>
	</span>
    <span class="name"><?=$arSection["NAME"]?></span>
</a>
<?}?>