<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//echo "<pre>";
//print_r($arResult);
?>

<div class="section-list">
	<?foreach ($arResult['SECTIONS'] as &$arSection) {
		$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
		$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);?>
		<div class="item" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
			<a class="photo" href="<? echo $arSection['SECTION_PAGE_URL']; ?>">
				<?if($arSection["PICTURE"]["SRC"]!=""){?>
					<img alt="<? echo $arSection['NAME']; ?>" title="<? echo $arSection['NAME']; ?>" src="<?=$arSection["PICTURE"]["SRC"]?>">
				<?} else {?>
					<img alt="<? echo $arSection['NAME']; ?>" title="<? echo $arSection['NAME']; ?>" src="/local/templates/index/images/no_photo.png">
				<?}?>
			</a>
			<a class="name" href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><span><? echo $arSection['NAME']; ?></span></a>
		</div>
	<?}?>
</div>