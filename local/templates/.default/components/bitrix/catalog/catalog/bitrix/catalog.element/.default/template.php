<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//if ($USER->IsAdmin()) echo '<pre>'.print_r($arResult, true).'</pre>';

?>

<div itemscope itemtype="http://schema.org/Product">

<h1 class="h1" itemprop="name"><?=$arResult["NAME"]?></h1>

<div class="mg-product-slides">

		<img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" itemprop="image">

	</div>  

<span itemprop="description" content="<?=$arResult["NAME"]?> по цене интернет-аптеки - купить в Москве недорого. Доставка для льготных категорий населения!"></span>

    
<div class="product-status">
  <div class="product-code">
          Артикул: <span class="label-article code" itemprop="productID">CN115</span>
        </div>

     

        <div  itemprop="offers" itemscope itemtype="http://schema.org/Offer">
      
		 		  
				
		 
        <ul class="product-status-list">
          <!--если не установлен параметр - старая цена, то не выводим его-->
          <?if($arResult["CATALOG_QUANTITY"]>0){?>
			<li> <span> <span  class="count esct">Товар есть в наличии<?=($arResult['ORIGINAL_PARAMETERS']['SHOW_MAX_QUANTITY']=='Y')?  ' ('.$arResult["CATALOG_QUANTITY"].' шт.)' : '' ?></span> </span></li>
          <?} else {?>    
            <li> <span> <span  class="count no">Позвоните нам, чтобы уточнить наличие</span> </span></li>         
          <?}?>  

          <?/*$APPLICATION->IncludeComponent(
            "bitrix:catalog.store.amount",
            "vell",
            Array(
                "STORES" => array(),
                "ELEMENT_ID" => $arResult["ID"],
                "ELEMENT_CODE" => "",
                "OFFER_ID" => "",
                "STORE_PATH" => "/store/store_detail.php",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000",
                "MAIN_TITLE" => "Наличие товара на складах",
                "USER_FIELDS" => array("",""),
                "FIELDS" => array("TITLE","ADDRESS","PHONE","SCHEDULE",""),
                "SHOW_EMPTY_STORE" => "Y",
                "USE_MIN_AMOUNT" => "Y",
                "SHOW_GENERAL_STORE_INFORMATION" => "N",
                "MIN_AMOUNT" => "0"
            )
        );*/?>  
        
          <?if($arResult["PROPERTIES"]["PROP1"]["VALUE"]!=""){?><li><?=$arResult["PROPERTIES"]["PROP1"]["NAME"]?>: <b><?=$arResult["PROPERTIES"]["PROP1"]["VALUE"]?>.</b></li><?}?>
          <?if($arResult["PROPERTIES"]["PROP2"]["VALUE"]!=""){?><li><?=$arResult["PROPERTIES"]["PROP2"]["NAME"]?>: <b><?=$arResult["PROPERTIES"]["PROP2"]["VALUE"]?>.</b></li><?}?>
          <?if($arResult["PROPERTIES"]["PROP3"]["VALUE"]!=""){?><li><?=$arResult["PROPERTIES"]["PROP3"]["NAME"]?>: <b><?=$arResult["PROPERTIES"]["PROP3"]["VALUE"]?>.</b></li><?}?>
          <?if($arResult["PROPERTIES"]["MNN"]["VALUE"]!=""){?><li><?=$arResult["PROPERTIES"]["MNN"]["NAME"]?>: <b><?=$arResult["PROPERTIES"]["MNN"]["VALUE"]?>.</b></li><?}?>

          <?if($arResult["PROPERTIES"]["POKAZ"]["VALUE_ENUM"]){?><li><?=$arResult["PROPERTIES"]["POKAZ"]["NAME"]?>: <b><?
             $i = count($arResult["PROPERTIES"]["POKAZ"]["VALUE_ENUM"]);
             foreach($arResult["PROPERTIES"]["POKAZ"]["VALUE_ENUM"] as $key => $value) {
                if($key<$i-1){ $value .=", ";} else {$value .=".";}
                echo strtolower($value);
             }
             echo '</b></li>';
          }?>
          <?if($arResult["PROPERTIES"]["VVEDENIE"]["VALUE_ENUM"]){?><li><?=$arResult["PROPERTIES"]["VVEDENIE"]["NAME"]?>: <b><?
             $i = count($arResult["PROPERTIES"]["VVEDENIE"]["VALUE_ENUM"]);
             foreach($arResult["PROPERTIES"]["VVEDENIE"]["VALUE_ENUM"] as $key => $value) {
                if($key<$i-1){ $value .=", ";} else {$value .=".";}
                echo strtolower($value);
             }
             echo '</b></li>';
          }?>

          <?if($arResult["PROPERTIES"]["PROP4"]["VALUE"]!=""){?><li><?=$arResult["PROPERTIES"]["PROP4"]["NAME"]?>: <b><?=$arResult["PROPERTIES"]["PROP4"]["VALUE"]?>.</b></li><?}?>
        </ul>

        <div class="product-price">
          <ul class="product-status-list ">
          
            <li>
              <div class="normal-price">
                
<span class="price"><i>Цена:</i> <span itemprop="price"><?=number_format($arResult["PRICES"]["BASE"]["VALUE"], 0, ',', ' ');?></span> <span itemprop="priceCurrency" content="RUB">руб.</span></span>
              </div>
            </li>
          </ul>
        </div>
        <a class="tovar-buy" onclick="yaCounter47622841.reachGoal('dobavkorzina'); return true;" href="<?=$arResult["BUY_URL"]?>">Купить</a>
 </div>
         
</div>

</div>


<div class="clear"></div>
<div class= "tabs">
	<? if(!empty($arResult["PROPERTIES"]["SEO_TEXT"]["~VALUE"])) { ?>
		<div class= "tabs-header">
			<div class="tab-h active-border" data-tab="0">Аптека "ВЕЛЛ"</div>
			<div class="tab-h" data-tab="1">Инструкция</div>
			<div class="tab-h"  id="review-count" data-tab="2">Отзывы</div>
		</div>
		<div class= "tabs-body">
			<div class="tab-b"  style="display:block"><?=$arResult["PROPERTIES"]["SEO_TEXT"]["~VALUE"]?> </div>
			<div class="tab-b"> <?=$arResult["~DETAIL_TEXT"]?>   </div>
	<? } else {?>
		<div class= "tabs-header">
			<div class="tab-h-single" data-tab="1">Инструкция</div>
		</div>
		<div class= "tabs-body">
			<div class="tab-b-single"> <!--noindex--><?=$arResult["~DETAIL_TEXT"]?><!--/noindex-->   </div>
		</div>
	<? } ?>
			<div class="tab-b">
						<h2 class="reviewstitle">Отзывы <?=$arResult["NAME"]?></h2>
					<?if(Bitrix\Main\Loader::includeModule('api.uncachedarea')&& Bitrix\Main\Loader::includeModule('api.reviews')) {
					  CAPIUncachedArea::includeFile("/reviews.php",
							 array(
								'ID'   => $arResult['ID'],
								'NAME' => $arResult['NAME'],
						 	)
					  );
					}
					?>
			</div>
	</div>

</div>
<?
$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"pokaz_list", 
	array(
		"COMPONENT_TEMPLATE" => "pokaz_list",
		"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
		"IBLOCK_ID" => $arParams['IBLOCK_ID'],
		"SECTION_ID" => "38",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "arrFilterPokaz",
		"USE_FILTER" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "N",
		"CUSTOM_FILTER" => "",
		"HIDE_NOT_AVAILABLE" => "Y",
		"HIDE_NOT_AVAILABLE_OFFERS" => "Y",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"PAGE_ELEMENT_COUNT" => "12",
		"LINE_ELEMENT_COUNT" => "3",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE_MOBILE" => "",
		"OFFERS_LIMIT" => "5",
		"BACKGROUND_IMAGE" => "-",
		"TEMPLATE_THEME" => "blue",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'3','BIG_DATA':false}]",
		"ENLARGE_PRODUCT" => "STRICT",
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
		"SHOW_SLIDER" => "Y",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"RCM_TYPE" => "personal",
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"SHOW_FROM_SECTION" => "N",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "N",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "N",
		"META_DESCRIPTION" => "-",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_FILTER" => "N",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"USE_PRODUCT_QUANTITY" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"ADD_TO_BASKET_ACTION" => "ADD",
		"DISPLAY_COMPARE" => "N",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"LAZY_LOAD" => "N",
		"LOAD_ON_SCROLL" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"COMPATIBLE_MODE" => "Y",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N"
	),
	$component
);?>
<script type="text/javascript">
jQuery( document ).ready(function() {
var reviewstitle = $('#reviews .api-block-top .api-block-left .api-reviews-count').text();
$('#review-count').append(' '+reviewstitle);
})
</script>