<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

	<h2 class="h3">Онкологические препараты</h2>


<div class="products-wrapper temp-style">
		<?foreach ($arResult["ITEMS"] as $key => $arItem) {
			//echo "<pre>";
			//print_r($arItem);
			?>
			<div class="tov_act_block">
		        <div class="block__img__tov">
		          <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
		           	<img class="mg-product-image" data-transfer="true" data-id="<?=$arItem['ID']?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">         
		           </a>
		       	</div>
		       	<div class="cont_tit">
		         	<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="tit__tovar"><?=$arItem["NAME"]?></a>
		           
		            <span class="desc__tov">
		            	Срок годности: <?=$arItem["PROPERTIES"]["PROP4"]["VALUE"]?><br>
		            	<?if($arItem["CATALOG_QUANTITY"]>0) {?>
		            		<i class="quantity-yes">Есть в наличии<?=($arResult['ORIGINAL_PARAMETERS']['SHOW_MAX_QUANTITY']=='Y')?  ' ('.$arItem["CATALOG_QUANTITY"].' шт.)' : '' ?></i>
		            	<?} else {?>
		            		<i class="quantity-no">На заказ</i>
		            	<?}?>
					</span>
		        </div>
		        
		        <a class="product-buy" href="<?=$arItem["ADD_URL"]?>">
		            <?=number_format($arItem["PRICES"]["BASE"]["VALUE"], 0, ',', ' ');?> <span class="sml">р</span>.
		        </a>

		        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="a__descrp">ОПИСАНИЕ</a>

		        <?if($arItem["PROPERTIES"]["NEW"]["VALUE"]!="") {?>
					<i class="news__sprt abs"></i>
				<?}?>
				<?if($arItem["PROPERTIES"]["STOCK"]["VALUE"]!="") {?>
					<i class="act__sprt abs"></i>
				<?}?>
		            
		    </div>
		<?}?>
		<div class="clear"></div>
	</div>
</div>
<?
if ($arParams['HIDE_SECTION_DESCRIPTION'] !== 'Y' && $arResult['NAV_RESULT']->NavPageNomer == 1)
{
	?>
	<div class="bx-section-desc bx-<?=$arParams['TEMPLATE_THEME']?>">
		<p class="bx-section-desc-post"><?=$arResult['DESCRIPTION']?></p>
	</div>
	<?
}
?>