<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			//echo "<pre>";
			//print_r($arItem);
			?>

			<div class="articles-index-block-item">
        <a class="articles-index-block-photo" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
          <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
        </a>
        <a class="articles-index-block-name" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
        <span class="articles-index-block-preview">
           <?=$arItem["PREVIEW_TEXT"]?> 
        </span>
       
      </div>

		<?endforeach;?>
