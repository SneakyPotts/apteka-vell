<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$item=0;
?>	
<h2>Производители</h2>
<ul class="brand-page">
<?foreach($arResult["ITEMS"] as $arItem):
$item++;
?>
	<?
	//echo "<pre>";
	//print_r($arItem);
	?>
<li class="brand-page__item brand-page__item_inner">
	<span class="brand-page__logo"><img src='<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>' alt=""></span>
	<a><?=$arItem["NAME"]?></a>
</li>	
<?endforeach;?>
</ul>
