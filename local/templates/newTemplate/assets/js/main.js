$(function(){
    $('.api-footer').remove();
    $('.catalog').on('click', function(){
        $('.catalog__drop-down').toggleClass('active')
    });
	/*Изменён класс с header__menu для срабатывания события только при нажатии на иконку бургера*/
    $('.header__menu-btn-container').on('click', function(){
        $(this).toggleClass('active');
        $('.header__menu ul').toggleClass('active');
    });
    $('.header__menu ul a').on('click', function(){
        $('.header__menu-btn').removeClass('active')
        $('.header__menu ul').removeClass('active')
    });



    $('a[data-target^="anchor"]').bind('click.smoothscroll', function(){
        var target = $(this).attr('href'),
            bl_top = $(target).offset().top -15;
        $('body, html').animate({scrollTop: bl_top}, 700);
        return false;
    });
});
$("body").on("click", ".minus", function () {
    var c = parseInt($(this).parents('#basket_quantity_control').find('input').val());
    var full = c == 1 ? 1 : c - 1;
    var id = parseInt($(this).parents('#basket_quantity_control').find('input').data('id'));
    reloadCart({id: id, quantity: full});
});

$("body").on("click", ".plus", function () {
    var c = parseInt($(this).parents('#basket_quantity_control').find('input').val());
    var full = c + 1;
    var id = parseInt($(this).parents('#basket_quantity_control').find('input').data('id'));
    var qual = $(this).data('quality');
    var max = $(this).data('max');
    if(qual === max){
        $('#maxQuality').modal('show');
        $('#modal-count').text(max);
    }else{
        reloadCart({id: id, quantity: full});
    }
});
$("body").on("change", ".quantity_value", function () {
    var full = $(this).val();
    var id = parseInt($(this).parents('#basket_quantity_control').find('input').data('id'));
    reloadCart({id: id, quantity: full});
});
function reloadCart(params) {
    $(".cart-block").css("opacity", 0.5);
    $("#allSum_FORMATED").css("opacity", 0.5);
    $.get("/cart/calc.php", params, function(html) {
        content = $(html).find("#basket_items");
        content_sum = $(html).find(".itog_summ");
        $(".cart-block").html(content).css("opacity", 1);
        $("#allSum_FORMATED").html(content_sum).css("opacity", 1);
    });
}
function reloadMiniCart() {
    $(".num-wrapper").css("opacity", 0.5);
    $(".basket__summ").css("opacity", 0.5);
    $.get("/cart/small_cart.php", function(html) {
        content = $(html).find(".numm");
        content_sum = $(html).find(".summ");
        $(".num-wrapper").html(content).css("opacity", 1);
        $(".basket__summ").html(content_sum).css("opacity", 1);
    });
}
$(document).ready(function() {
    $('.control li').click(function() {
        $('.control li').removeClass('active');
        $(this).addClass('active');
    });

    $('.askBtn').on('click',function(){
        var prodId = $(this).data('id');
        var prodTitle = $(this).data('title');
        $('#askModal').find('textarea[name="form_textarea_23"]').html(prodTitle);
        $('#askModal').find('input[name="form_hidden_24"]').val(prodId);
        $('#askModal').find('input[name="form_hidden_25"]').val(prodTitle);
    });

    $('.short-info .where a').on('click',function(){
        $('.control li[data-to="apteka"]').trigger('click');
    });

    $('.short-info .instruction a').on('click',function(){
        $('.control li[data-to="instr"]').trigger('click');
    });
    $('.short-info .reviews-count a').on('click',function(){
        $('.control li[data-to="rev"]').trigger('click');
    });
});

$(document).ready(function(){
    $('input[name="userphone"]').mask("+7 (999) 999-9999");
    $('input[name="form_text_14"]').mask("+7 (999) 999-9999");
    $('input[name="form_text_19"]').mask("+7 (999) 999-9999");
    $('input[name="form_text_22"]').mask("+7 (999) 999-9999");
    $('#show-filter').click(function () {
        $('.left-filter').toggle();
    });
    $('#show-instr').click(function () {
        $('.instr-text').css('height','auto');
        $(this).hide();
    });
    $('#show-apteka').click(function () {
        $('.apteka-text').css('height','auto');
        $(this).hide();
    });
    $('.add2cart').on('click',function(e){
        reloadMiniCart();
        e.preventDefault();
        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: "/ajax/add2cart.php",
            data: {
                'id': id,
            },
            dataType: "json",
            success:function(data){
                if(data.success) {
                    BX.onCustomEvent('OnBasketChange');
                    $('#add2cartModal').modal('show');
                } else {
                    console.log(data.errors);
                }
            },
            error:function(xhr,str){
                console.log(xhr.responseCode);
            }
        });
        return false;
    });


    $('#checkout').on('submit',function(e){
        e.preventDefault();
        if(!validateForm(this)) return false;
        var curform = $(this);
        var formentry = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/ajax/checkout.php",
            data: formentry,
            dataType: "json",
            success:function(data){
                if(data.success) {
                    // console.log(data);
                    // document.location.reload(true);
                    // $('#successModal').modal('show');
                    $('#orderModal').modal('show');
                    setTimeout('window.location.replace("/personal/")', 2000);
                } else {
                    console.log(data.errors);
                }
            },
            error:function(xhr,str){
                console.log(xhr.responseCode);
            }
        });
        return false;
    });
    $('#askForm').on('submit',function(e){
        e.preventDefault();
        if(!validateForm(this)) return false;
        var curform = $(this);
        var formentry = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/ajax/ask.php",
            data: formentry,
            dataType: "json",
            success:function(data){
                if(data.success) {
                    // console.log(data);
                    // document.location.reload(true);
                    $('#successModal').modal('show');
                } else {
                    console.log(data.errors);
                }
            },
            error:function(xhr,str){
                console.log(xhr.responseCode);
            }
        });
        return false;
    });


    $('#addReview').on('submit',function(e){
        e.preventDefault();
        if(!validateForm(this)) return false;
        var curform = $(this);
        var formentry = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/ajax/addreview.php",
            data: formentry,
            dataType: "json",
            success:function(data){
                if(data.success) {
                    $('#successModal').modal('show');
                    // document.location.reload(true);
                } else {
                    console.log(data.errors);
                }
            },
            error:function(xhr,str){
                console.log(xhr.responseCode);
            }
        });
        return false;
    });

    $('#callback').on('submit',function(e){
        e.preventDefault();
        if(!validateForm(this)) return false;
        var curform = $(this);
        var formentry = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/ajax/callback.php",
            data: formentry,
            dataType: "json",
            success:function(data){
                if(data.success) {
                    $('#successModal').modal('show');
                    // document.location.reload(true);
                } else {
                    console.log(data.errors);
                }
            },
            error:function(xhr,str){
                console.log(xhr.responseCode);
            }
        });
        return false;
    });
    $('#callback2').on('submit',function(e){
        e.preventDefault();
        if(!validateForm(this)) return false;
        var curform = $(this);
        var formentry = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/ajax/callback.php",
            data: formentry,
            dataType: "json",
            success:function(data){
                if(data.success) {
                    $('#successModal').modal('show');
                    // document.location.reload(true);
                } else {
                    console.log(data.errors);
                }
            },
            error:function(xhr,str){
                console.log(xhr.responseCode);
            }
        });
        return false;
    });
    $('.filter__btn').on('click',function(){
        $('.catalog__leftside').toggle();
    });
});