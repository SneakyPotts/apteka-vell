<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(IS_MAIN!='Y'&&IS_CATALOG!='Y'){?>
    </div>
<?}?>
<?if(IS_MAIN!='Y'){?>
    </div></main>
<?}?>
<?if(!empty($GLOBALS['seo'])){?>
    <section class="seo-text block-text">
        <div class="container">
            <?=htmlspecialchars_decode($GLOBALS['seo'])?>
        </div>
    </section>
<?}?>

<!-- Наши преимущества и "Мы вам перезвоним" (закомментировано) -->
<div class="top__section row" id="price">
    <div class="container">
        <div class="advantages col-12">
			<?$APPLICATION->IncludeComponent('bitrix:main.include','',array('AREA_FILE_SHOW'=>'file','PATH'=>SITE_TEMPLATE_PATH.'/include/main/main_icons.php'));?>
        </div>
        <!-- Форма обратной связи для "Мы вам перезвоним"-->
        <!--            <div class="form col-6">-->
        <!--                --><?//$APPLICATION->IncludeComponent('bitrix:main.include','',array('AREA_FILE_SHOW'=>'file','PATH'=>SITE_TEMPLATE_PATH.'/include/call_form.php'));?>
        <!--            </div>-->
    </div>
</div>
<?//if(IS_MAIN!='Y'){?>
<?//}?>

<!--Зелёный блок с текстом-->
<div class="" style="display: flex; justify-content: center; margin-top: 1rem; margin-bottom: 0 !important; background-color: #71bc06; width: 100%; padding: 25px 0 25px 0;">
    <p style="margin-bottom: 0 !important; text-align: center;">Доставка лекарственных препаратов для медицинского применения, которые отпускаются по рецепту врача, не осуществляется</p>
</div>


   <footer class="footer" id="contact" style="padding-top: 20px;">
       <div class="container">
           <div>
               <a href="/" class="logo-2">
				   <? $APPLICATION->IncludeComponent('bitrix:main.include', '', array('AREA_FILE_SHOW' => 'file', 'PATH' => SITE_TEMPLATE_PATH . '/include/contacts/1.php')); ?>
               </a>
           </div>
           <div class="row">
               <div class="col-12 col-sm-4 col-lg-3">
                   <div class="row justify-content-end footer__contact align-items-end">
					   <? $APPLICATION->IncludeComponent('bitrix:main.include', '', array('AREA_FILE_SHOW' => 'file', 'PATH' => SITE_TEMPLATE_PATH . '/include/contacts/2.php')); ?>
                       <div class="col-lg-auto schedule"><? $APPLICATION->IncludeComponent('bitrix:main.include', '', array('AREA_FILE_SHOW' => 'file', 'PATH' => SITE_TEMPLATE_PATH . '/include/contacts/3.php')); ?></div>
                       <div class="col-lg-auto"><? $APPLICATION->IncludeComponent('bitrix:main.include', '', array('AREA_FILE_SHOW' => 'file', 'PATH' => SITE_TEMPLATE_PATH . '/include/contacts/4.php')); ?></div>
                   </div>
               </div>
               <style>
                   .footer-list {
                       margin-top: -70px;
                   }
                   @media screen and (max-width: 576px) {
                       .footer-list {
                           margin-top: 0;
                       }
                   }
               </style>
               <div class="col-12 col-sm-4 col-lg-3 footer__contact footer__menu ml-3 ml-sm-0 footer-list">
                   <a href="/lekarstvennye-sredstva/onkologicheskie-preparaty/" class="links links_footer">
                       Каталог лекарств от рака
                   </a>
                   <a href="/about/" class="links links_footer">
                       О компании
                   </a>
                   <a href="/blog/" class="links links_footer">
                       Блог
                   </a>
                   <a href="/contacts/" class="links links_footer">
                       Контакты
                   </a>
<!--                   <a href="/soglashenie.php" class="links links_footer">-->
<!--                       Пользовательское соглашение-->
<!--                   </a>-->
<!--                   <a href="/personalnie-dannie.php" class="links links_footer">-->
<!--                       Обработка персональных данных и файлы cookie-->
<!--                   </a>-->
<!--                   <a href="/politika-confidencialnosti.php" class="links links_footer">-->
<!--                       Политика конфиденциальности-->
<!--                   </a>-->

               </div>
<!--               <div class="form col-12 col-sm-6 mt-sm-5 mt-lg-0 footer_form">-->
<!--                   <div class="form__title">-->
<!--					   --><?// $APPLICATION->IncludeComponent('bitrix:main.include', '', array('AREA_FILE_SHOW' => 'file', 'PATH' => SITE_TEMPLATE_PATH . '/include/form1/1.php')); ?>
<!--                   </div>-->
<!--                   <div class="form__subtitle">-->
<!--					   --><?// $APPLICATION->IncludeComponent('bitrix:main.include', '', array('AREA_FILE_SHOW' => 'file', 'PATH' => SITE_TEMPLATE_PATH . '/include/form1/2.php')); ?>
<!--                   </div>-->
<!--                   <form method="post" id="callback">-->
<!--                       <input type="text" placeholder="Ваше имя *" name="username" data-required="1">-->
<!--                       <input type="tel" placeholder="Ваш телефон *" name="userphone" data-required="1">-->
<!--                       <button type="submit">Заказать звонок</button>-->
<!--                       <textarea placeholder="Сообщение" name="comment"></textarea>-->
<!--                   </form>-->
<!--               </div>-->
           </div>
       </div>
        <style>
            .footer__bottom .container p {
                margin-top: revert;
            }
        </style>
        <div class="footer__bottom">
            <div class="container">
                <?$APPLICATION->IncludeComponent('bitrix:main.include','',array('AREA_FILE_SHOW'=>'file','PATH'=>SITE_TEMPLATE_PATH.'/include/footer/11.php'));?>
            </div>
            
        </div>
    </footer>

<!-- Кнопка "Обратная связь" в хедере вызывает попап -->
<div class="modal fade" id="callbackModal" style="opacity: 1;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
         <h4 class="modal-title review-title">Обратная связь</h4>
          <?$APPLICATION->IncludeComponent("bitrix:form.result.new","callback",Array(
                  "CACHE_TIME" => "3600",
                  "CACHE_TYPE" => "A",
                  "AJAX_MODE" => "Y",
                  "AJAX_OPTION_SHADOW" => "N",
                  "AJAX_OPTION_JUMP" => "Y",
                  "AJAX_OPTION_STYLE" => "Y",
                  "AJAX_OPTION_HISTORY" => "N",
                  "CHAIN_ITEM_LINK" => "",
                  "CHAIN_ITEM_TEXT" => "",
                  "EDIT_URL" => "",
                  "IGNORE_CUSTOM_TEMPLATE" => "Y",
                  "LIST_URL" => "",
                  "SEF_MODE" => "N",
                  "SUCCESS_URL" => "",
                  "USE_EXTENDED_ERRORS" => "N",
                  "VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
                  "WEB_FORM_ID" => "6"
              )
          );?>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<!-- Кнопка "трубка" на карточке товара на вызывает попап -->
<div class="modal fade" id="askModal" style="opacity: 1;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
         <h4 class="modal-title review-title">Узнать о товаре<br><span class="askTitle" style="color:#A01220"></span></h4>
          <?$APPLICATION->IncludeComponent(
                  "bitrix:form.result.new",
                  "callback",
                  Array(
                      "CACHE_TIME" => "3600",
                      "CACHE_TYPE" => "A",
                      "AJAX_MODE" => "Y",
                      "AJAX_OPTION_SHADOW" => "N",
                      "AJAX_OPTION_JUMP" => "Y",
                      "AJAX_OPTION_STYLE" => "Y",
                      "AJAX_OPTION_HISTORY" => "N",
                      "CHAIN_ITEM_LINK" => "",
                      "CHAIN_ITEM_TEXT" => "",
                      "EDIT_URL" => "",
                      "IGNORE_CUSTOM_TEMPLATE" => "Y",
                      "LIST_URL" => "",
                      "SEF_MODE" => "N",
                      "SUCCESS_URL" => "",
                      "USE_EXTENDED_ERRORS" => "N",
                      "VARIABLE_ALIASES" =>
                          Array(
                                "RESULT_ID"=>"RESULT_ID",
                                "WEB_FORM_ID"=>"WEB_FORM_ID"
                               ),
                      "WEB_FORM_ID" => "7"
              )
          );?>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>
<!-- Кнопка "корзина" на карточке товара вызывает попап -->
<div class="modal fade" id="add2cartModal" style="opacity: 1;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title review-title">Товар успешно добавлен</h4><br>
                Товар успешно добавлен в корзину
            </div>
            <div class="modal-footer">
                <a href class="message_catalog" data-dismiss="modal">Продолжить покупки</a>
                <a href="/cart/" class="message_cart">Перейти в корзину</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="maxQuality">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title review-title">Максимальное количество</h4>
                <p>В наличии есть только <span id="modal-count">N</span> единиц данного препарата.<br> Свяжитесь с <a href="/contacts/">нами</a>, чтобы заказать больше.</p>
            </div>
        </div>
    </div>
</div>

<!-- Липкий блок внизу сайта-->
<style>
    .sticky_bloc {
        position: sticky;
        bottom: 0;
        z-index: 99;
        background: rgba(243,243,243,0.85);
    }
    .paragraph {
        margin: 0;
        text-align: center;
        text-transform: uppercase;
    }
    .p1 {
        padding: 5px 0 2px 0;
        font-size: 32px;
        line-height: 32px;
    }
    .p2 {
        padding: 2px 0 7px 0;
        font-size: 24px;
        line-height: 24px;
    }
    @media screen and (max-width: 990px) {
        .p1 {
            font-size: 26px;
            line-height: 26px;
        }
        .p2 {
            font-size: 20px;
            line-height: 20px;
        }
    }
    @media screen and (max-width: 800px) {
        .p1 {
            font-size: 18px;
            line-height: 24px;
        }
        .p2 {
            font-size: 16px;
            line-height: 18px;
        }
    }
    @media screen and (max-width: 601px) {
        .p1 {
            font-size: 15px;
            line-height: 15px;
        }
        .p2 {
            font-size: 14px;
            line-height: 14px;
        }    }
    @media screen and (max-width: 470px) {
        .p1 {
            font-size: 10px;
            line-height: 10px;
        }
        .p2 {
            font-size: 9px;
            line-height: 9px;
        }    }
    @media screen and (max-width: 370px) {
        .p1 {
            font-size: 8px;
            line-height: 8px;
        }
        .p2 {
            font-size: 7px;
            line-height: 7px;
        }
    }
</style>
<div class="sticky_bloc">
    <p class="paragraph p1">Самолечение может быть опасным для вашего здоровья</p>
    <p class="paragraph p2"> Перед применением проконсультируйтесь со специалистом!</p>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/assets/js/libs.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/assets/js/main.js"></script>


<!-- Slick slider -->
<!--<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>-->
<script src="<?=SITE_TEMPLATE_PATH?>/slick_slide/slick.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    $('.responsive').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1250,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 900,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 680,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
</script>
<!-- Slick slider -->

<!-- Блок уведомления об использовании куков -->
<?
		$APPLICATION->IncludeComponent(
			"navigator:notificblock",
			"",
			array(
				"SETTINGS_BACKGRAUND_COLOR" => "#333",
				"SETTINGS_BACKGRAUND_opacity" => "1",
				"SETTINGS_BUTTON" => "Y",
				"SETTINGS_BUTTON_BACKGRAUND_COLOR" => "#515151",
				"SETTINGS_BUTTON_TEXT" => "ОК",
				"SETTINGS_BUTTON_TEXT_COLOR" => "#fff",
				"SETTINGS_TEXT" => "Пользуясь нашим сайтом, вы соглашаетесь с тем, что мы используем cookies. <a href='/informatsiya/politika-confidencialnosti/'>Подробнее...<a>",
				"SETTINGS_TEXT_COLOR" => "#fff",
				"SETTINGS_TYPE" => "right",
				"SETTINGS_UUID_CCOOKIE_JS" => "eyp",
				"jQuery_on" => "N"
			)
		);
?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(47622841, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47622841" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>