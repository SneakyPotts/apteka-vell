<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>
	<div class="col-md-2">
		<div class="catItem__img">
			<a href="<?=$item['DETAIL_PAGE_URL']?>">
				<img src="<?=$item['PREVIEW_PICTURE']['SRC']?>">
			</a>
		</div>
	</div>
	<div class="col-md-5 col-lg-6">
		<div class="catItem__info">
			<a href="<?=$item['DETAIL_PAGE_URL']?>"><span class="catItem__title"><?=$item['NAME']?></span></a>
			<span class="catItem__subtitle"><?=$item['NAME']?></span>
			<ul>
				<?if(!empty($item['PROPRETIES']['PROP4']['VALUE'])){?><li>Срок годности: <span><?=$item['PROPRETIES']['PROP4']['VALUE']?></span></li><?}?>
				<?if(!empty($item['PROPRETIES']['PROP1']['VALUE'])){?><li>Производитель: <span><?=$item['PROPRETIES']['PROP1']['VALUE']?></span></li><?}?>
			</ul>
			<?foreach($item['PROPRETIES'] as $arProp){?>
			<?}?>
		</div>
	</div>
	<div class="col-sm-12 col-md-3 col-lg-2">
		<div class="catItem__price">
			<span><?=number_format($item['PRICES']['BASE']['VALUE'],0,',',' ');?> ₽</span>
            <?if($item['CATALOG_QUANTITY']>0){?>
            <p class="avail">В наличии</p>
            <?}else{?>
            <p class="zakaz">На заказ</p>
            <?}?>
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <div class="catItem__buy">
          <?if($item['CATALOG_QUANTITY']>0){?>
            <a href="?action=ADD2BASKET&id=<?=$item['ID']?>" class="add2cart" data-id="<?=$item['ID']?>"><img src="<?=SITE_TEMPLATE_PATH?>/assets/images/buy1.png"></a>
            <?}else{?>
            <a href="javascript:void(0);" class="askBtn" data-toggle="modal" data-target="#askModal" data-title="<?=$item['NAME']?>" data-id="<?=$item['ID']?>">
                <img class="askImg" src="<?=SITE_TEMPLATE_PATH?>/assets/images/icons/ask2.png" alt="<?=$item['NAME']?>_buy">
            </a>
            <?}?>
		</div>
	</div>

<?
// echo '<pre>'; print_r($item['PRICES']['BASE']['CAN_BUY']); echo '</pre>';
// echo '<pre>'; print_r($item['PRICES']['BASE']['PRINT_VALUE']); echo '</pre>';
?>