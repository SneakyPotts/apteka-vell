<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?> 
<div class="preparations__inner row row-flex">
<?foreach ($arResult["ITEMS"] as $key => $arItem) {?>
	<div class="preparations__item col">
        <?$file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>140, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);?>
        <a class="img_block" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$file["src"]?>" alt="<?echo mb_strimwidth($arItem["NAME"], 0, 10, "...");?>"></a>
		<div class="preparations__item-title">
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
		</div>
		<div class="preparations__item-subtitle">
			Срок годности: <?=$arItem["PROPERTIES"]["PROP4"]["VALUE"]?>
		</div>
		<div class="preparations__item-block">
			<div class="preparations__item-price">
				<?=number_format($arItem["PRICES"]["BASE"]["VALUE"], 0, ',', ' ');?> ₽
			</div>
			<?if($arItem["CATALOG_QUANTITY"]>0) {?>
				<?//=$arItem["CATALOG_QUANTITY"]?>
			<?} else {?>
				
			<?}?>
			<?if($arItem["PROPERTIES"]["NEW"]["VALUE"]!="") {?><?}?>
			<?if($arItem["PROPERTIES"]["STOCK"]["VALUE"]!="") {?><?}?>
			
			<?if($arItem["CATALOG_QUANTITY"]>0) {?>
			<a href="<?=$arItem["ADD_URL"]?>" class="preparations__item-link add2cart" data-id="<?=$arItem['ID']?>">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/images/icons/add-basket.png" alt="<?=$arItem['NAME']?>_buy">
			</a>
			<?} else {?>
			<a href="javascript:void(0);" class="preparations__item-link askBtn" data-toggle="modal" data-target="#askModal" data-title="<?=$arItem['NAME']?>" data-id="<?=$arItem['ID']?>">
				<img class="askImg" src="<?=SITE_TEMPLATE_PATH?>/assets/images/icons/ask2.png" alt="<?=$arItem['NAME']?>_mess">
			</a>	
			<?}?>
			
		</div>


	</div>
<?}?>

</div>
