<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Loader;
use \Redsign\EShop24\ParametersUtils;
use \Redsign\EShop24\LazyloadUtils;

$arViewModeList = array(/*'LIST', 'LINE', 'TEXT', 'TILE',*/ 'THUMB');

$columnCountList = array('1', '2', '3', '4', '6', '12');

$arDefaultParams = array(
	'VIEW_MODE' => 'THUMB',
	'SHOW_PARENT_NAME' => 'Y',
	'HIDE_SECTION_NAME' => 'N',
// 	'LIST_COLUMNS_COUNT' => '6'
);

$arParams = array_merge($arDefaultParams, $arParams);

if (!in_array($arParams['VIEW_MODE'], $arViewModeList))
	$arParams['VIEW_MODE'] = 'THUMB';
if ('N' != $arParams['SHOW_PARENT_NAME'])
	$arParams['SHOW_PARENT_NAME'] = 'Y';
if ('Y' != $arParams['HIDE_SECTION_NAME'])
	$arParams['HIDE_SECTION_NAME'] = 'N';
// if (!in_array($arParams['LIST_COLUMNS_COUNT'], $columnCountList))
// 	$arParams['LIST_COLUMNS_COUNT'] = '6';

if (Loader::includeModule('redsign.eshop24'))
{
	$arParams['GRID_RESPONSIVE_SETTINGS'] = ParametersUtils::prepareGridSettings($arParams['~GRID_RESPONSIVE_SETTINGS']);
}

$arParams['LVL1_SECTION_COUNT'] = isset($arParams['LVL1_SECTION_COUNT']) ? (int)$arParams['LVL1_SECTION_COUNT'] : 0;

$arParams['VIEW_MODE'] = in_array($arParams['VIEW_MODE'], $arViewModeList)
	? $arParams['VIEW_MODE']
	: 'TILE';

$arParams['SHOW_PARENT_NAME'] = isset($arParams['SHOW_PARENT_NAME']) && $arParams['SHOW_PARENT_NAME'] === 'N' ? 'N' : 'Y';
$arParams['HIDE_SECTION_NAME'] = isset($arParams['HIDE_SECTION_NAME']) && $arParams['HIDE_SECTION_NAME'] === 'N' ? 'N' : 'Y';

$arParams['PARENT_TITLE'] = isset($arParams['PARENT_TITLE']) ? trim($arParams['PARENT_TITLE']) : '';

$arResult['VIEW_MODE_LIST'] = $arViewModeList;

if (0 < $arResult['SECTIONS_COUNT'])
{
	if (!in_array($arParams['VIEW_MODE'], array('TILE')))
	{
		$boolClear = false;
		$arNewSections = array();
		foreach ($arResult['SECTIONS'] as &$arOneSection)
		{
			if (1 < $arOneSection['RELATIVE_DEPTH_LEVEL'])
			{
				$boolClear = true;
				continue;
			}
			$arNewSections[] = $arOneSection;
		}
		unset($arOneSection);
		if ($boolClear)
		{
			$arResult['SECTIONS'] = $arNewSections;
			$arResult['SECTIONS_COUNT'] = count($arNewSections);
		}
		unset($arNewSections);
	}
}

if (0 < $arResult['SECTIONS_COUNT'])
{
	$boolPicture = false;
	$boolDescr = false;
	$arSelect = array('ID');
	$arMap = array();
	if (in_array($arParams['VIEW_MODE'], array('LINE', 'TILE', 'THUMB')))
	{
		reset($arResult['SECTIONS']);
		$arCurrent = current($arResult['SECTIONS']);
		if (!isset($arCurrent['PICTURE']))
		{
			$boolPicture = true;
			$arSelect[] = 'PICTURE';
		}
		if ('LINE' == $arParams['VIEW_MODE'] && !array_key_exists('DESCRIPTION', $arCurrent))
		{
			$boolDescr = true;
			$arSelect[] = 'DESCRIPTION';
			$arSelect[] = 'DESCRIPTION_TYPE';
		}
	}
	if ($boolPicture || $boolDescr)
	{
		foreach ($arResult['SECTIONS'] as $key => $arSection)
		{
			$arMap[$arSection['ID']] = $key;
		}
		unset($key, $arSection);

		$rsSections = CIBlockSection::GetList(array(), array('ID' => array_keys($arMap)), false, $arSelect);
		while ($arSection = $rsSections->GetNext())
		{
			if (!isset($arMap[$arSection['ID']]))
				continue;
			$key = $arMap[$arSection['ID']];
			if ($boolPicture)
			{
				$arSection['PICTURE'] = intval($arSection['PICTURE']);
				$arSection['PICTURE'] = (0 < $arSection['PICTURE'] ? CFile::GetFileArray($arSection['PICTURE']) : false);
				$arResult['SECTIONS'][$key]['PICTURE'] = $arSection['PICTURE'];
				$arResult['SECTIONS'][$key]['~PICTURE'] = $arSection['~PICTURE'];
			}
			if ($boolDescr)
			{
				$arResult['SECTIONS'][$key]['DESCRIPTION'] = $arSection['DESCRIPTION'];
				$arResult['SECTIONS'][$key]['~DESCRIPTION'] = $arSection['~DESCRIPTION'];
				$arResult['SECTIONS'][$key]['DESCRIPTION_TYPE'] = $arSection['DESCRIPTION_TYPE'];
				$arResult['SECTIONS'][$key]['~DESCRIPTION_TYPE'] = $arSection['~DESCRIPTION_TYPE'];
			}
		}
		unset($arSection);
	}
}

if (
	($arParams['SHOW_PARENT_NAME'] == 'Y' || $arParams['SHOW_PARENT_DESCR'] == 'Y')
	&& $arResult['SECTION']['ID'] == 0
)
{
	$arOrder = [];
	$arFilter = [
		'TYPE' => $arParams['IBLOCK_TYPE'],
		'ID' => $arParams['IBLOCK_ID'],
	];
	$bIncCnt = false;

	$dbIblock = CIBlock::getList($arOrder, $arFilter, $bIncCnt);

	if ($arIblock = $dbIblock->getNext())
	{
		$arResult['SECTION']['NAME'] = $arIblock['NAME'];
		$arResult['SECTION']['DESCRIPTION'] = $arIblock['DESCRIPTION'];
	}
	unset($arOrder, $arFilter, $bIncCnt);
}
