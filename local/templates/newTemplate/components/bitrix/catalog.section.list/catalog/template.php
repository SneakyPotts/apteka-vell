<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Redsign\EShop24\ParametersUtils;

// $arViewModeList = $arResult['VIEW_MODE_LIST'];

// $arViewStyles = array(
	// 'LINE' => array(
		// 'CONT' => 'catalog_sections catalog_sections-line',
		// 'TITLE' => 'catalog_sections__title h3',
		// 'DESCRIPTION' => 'catalog_sections__descr',
		// 'LIST' => 'catalog_sections__list-line row',
		// 'EMPTY_IMG' => $this->GetFolder().'/images/no_photo.png'
	// ),
	// 'BANNER' => array(
		// 'CONT' => 'catalog_sections',
		// 'TITLE' => 'catalog_sections__title h3',
		// 'DESCRIPTION' => 'catalog_sections__descr',
		// 'LIST' => 'catalog_sections__list-banners row',
		// 'EMPTY_IMG' => $this->GetFolder().'/images/no_photo.png'
	// ),
	// 'TILE' => array(
		// 'CONT' => 'catalog_sections',
		// 'TITLE' => 'catalog_sections__title h3',
		// 'DESCRIPTION' => 'catalog_sections__descr',
		// 'LIST' => 'catalog_sections__list row',
		// 'EMPTY_IMG' => $this->GetFolder().'/images/no_photo.png'
	// ),
	// 'THUMB' => array(
		// 'CONT' => 'catalog_sections',
		// 'TITLE' => 'catalog_sections__title h3',
		// 'DESCRIPTION' => 'catalog_sections__descr',
		// 'LIST' => 'catalog_sections__list-banners row',
		// 'EMPTY_IMG' => $this->GetFolder().'/images/no_photo.png'
	// ),
// );
// $arCurView = $arViewStyles[$arParams['SECTIONS_VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

$this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
$this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

if (0 < $arResult["SECTIONS_COUNT"])
{
?>
<ul class="row list-unstyled" id="<?=$this->GetEditAreaId($arResult['SECTION']['ID']);?>">
<?
	switch ($arParams['SECTIONS_VIEW_MODE'])
	{
		case 'THUMB':
		default:
			$iLvl1SectionCount = 0;

			$sGridClass = '';
			if (Loader::includeModule('redsign.eshop24'))
				$sGridClass = ParametersUtils::gridToString($arParams['GRID_RESPONSIVE_SETTINGS']);

			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

					if ($arParams['LVL1_SECTION_COUNT'] > 0 && $arParams['LVL1_SECTION_COUNT'] < $iLvl1SectionCount)
					{
						$intCurrentDepth = 0;
						break;
					}
					if (empty($arSection['PICTURE']))
						$arSection['PICTURE'] = array(
							'SRC' => $this->GetFolder().'/images/no_photo.png',
							'ALT' => (
								'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
								? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
								: $arSection["NAME"]
							),
							'TITLE' => (
								'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
								? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
								: $arSection["NAME"]
							)
						);
					?>
					<li class="<?=$sGridClass?> mb-3 col-12 col-md-4" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
						<a class="section-item section-item--thumb text-reset text-decoration-none d-block h-100" href="<?=$arSection['SECTION_PAGE_URL']?>">
							<div class="row align-items-center h-100">
								<div class="col-auto">
									<div class="section-item__image-wrapper">
										<img class="section-item__image" src="<?=$arSection['PICTURE']['SRC']?>" alt="<?=$arSection['PICTURE']['ALT']?>" loading="lazy">
									</div>
								</div>
								<div class="col">
									<?php if ('Y' != $arParams['HIDE_SECTION_NAME']): ?>
										<h4 class="section-item__title">
											<?php
											echo $arSection['NAME'];
											if ($arParams['COUNT_ELEMENTS'] && $arSection['ELEMENT_CNT'] > 0)
											{
												?> <span>(<?=$arSection['ELEMENT_CNT']?>)</span><?
											}
											?>
										</h4>
									<?php endif; ?>
								</div>
							</div>
						</a>
					</li>
				<?php
			}
			unset($arSection);
			break;
	}
?>
</ul>
<?
}
