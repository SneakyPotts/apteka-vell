<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
    <style>
        .footer_form {
            /*padding: 10px;*/
        }

        .footer_form form {
            justify-content: space-between;
            margin: 0;
        }
        .callback_section {
            width: 48%;
            margin: 0;
        }
        .callback_section.SIMPLE_MESSAGE_CALL {
            width: 100%;
        }

        .callback_section input.inputtext {
            width: 100%;
        }

        .callback_button {
            padding: 0 !important;
            background-color: #71bc06 !important;
            color: #000 !important;
        }
    </style>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y")
{
    ?>
    <?=$arResult["FORM_HEADER"]?>
        <?
        foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
        {
            if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
            {
                echo $arQuestion["HTML_CODE"];
            }
            else
            {
                ?>
                <div class="form-group callback_section <?= $FIELD_SID?>">
                    <label for="">
                        <?=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?>
                        <?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
                            <span class="error-fld" title="<?=htmlspecialcharsbx($arResult["FORM_ERRORS"][$FIELD_SID])?>"></span>
                        <?endif;?>
                    </label>
                    <?=$arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />".$arQuestion["IMAGE"]["HTML_CODE"] : ""?>

                    <?=$arQuestion["HTML_CODE"]?>
                </div>
                <?
            }
        } //endforeach
        ?>
            <input <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" class="form-control addReviewBtn callback_button" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />
    <?=$arResult["FORM_FOOTER"]?>
    <?
} //endif (isFormNote)
?>
<script>
        document.querySelector(".SIMPLE_QUESTION_891 label").setAttribute("hidden", "");
        document.querySelector(".SIMPLE_QUESTION_220 label").setAttribute("hidden", "");
        document.querySelector(".SIMPLE_MESSAGE_CALL label").setAttribute("hidden", "");
        document.querySelector(".SIMPLE_MESSAGE_CALL textarea").setAttribute("placeholder", "Комментарий");
        document.querySelector(".SIMPLE_QUESTION_891 input").setAttribute("placeholder", "Ваше имя*");
        document.querySelector(".SIMPLE_QUESTION_220 input").setAttribute("placeholder", "Ваш телефон*");

</script>
