<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="col-lg-auto main_menu__list" style="width: auto;">

<?
foreach($arResult as $key => $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
    <?if ($arResult[$key]['TEXT'] == "Информация") {?>

	<?if($arItem["SELECTED"]):?>
        <li class="main_menu__item arrow">
            <a class="main_menu__link" href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a>
			<?$APPLICATION->IncludeComponent(
				"bitrix:menu",
				"new_multilevel",
				array(
					"COMPONENT_TEMPLATE" => "tree",
					"ROOT_MENU_TYPE" => "left",
					"MENU_CACHE_TYPE" => "N",
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_CACHE_GET_VARS" => array(
					),
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "left",
					"USE_EXT" => "N",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N"
				),
				false
			);?>
        </li>
	<?else:?>
		<li class="main_menu__item arrow">
            <a class="main_menu__link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
			<?$APPLICATION->IncludeComponent(
				"bitrix:menu",
				"new_multilevel",
				array(
					"COMPONENT_TEMPLATE" => "tree",
					"ROOT_MENU_TYPE" => "left",
					"MENU_CACHE_TYPE" => "N",
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_CACHE_GET_VARS" => array(
					),
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "left",
					"USE_EXT" => "N",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N"
				),
				false
			);?>
        </li>
	<?endif?>

    <? } else {?>

    <?if($arItem["SELECTED"]):?>
		<li  class="main_menu__item">
            <a class="main_menu__link" href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a>
        </li>
	<?else:?>
		<li  class="main_menu__item">
            <a class="main_menu__link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
        </li>
	<?endif?>

    <?}?>
	
<?endforeach?>


	<li class="tel-phone"><a href="#" class="col-lg-auto col-md-auto phone"><?$APPLICATION->IncludeComponent('bitrix:main.include','',array('AREA_FILE_SHOW'=>'file','PATH'=>SITE_TEMPLATE_PATH.'/include/header/phone1.php'));?></a></li
		class="tel-phone">
	<li class="tel-phone"><a href="#" class="col-lg-auto col-md-auto "><?$APPLICATION->IncludeComponent('bitrix:main.include','',array('AREA_FILE_SHOW'=>'file','PATH'=>SITE_TEMPLATE_PATH.'/include/header/phone2.php'));?></a></li>
</ul>
<?endif?>


