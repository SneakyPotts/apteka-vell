<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="col-lg-auto">

<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
		<li><a href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
		<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
<?endforeach?>


	<li class="tel-phone"><a href="#" class="col-lg-auto col-md-auto phone"><?$APPLICATION->IncludeComponent('bitrix:main.include','',array('AREA_FILE_SHOW'=>'file','PATH'=>SITE_TEMPLATE_PATH.'/include/header/phone1.php'));?></a></li
		class="tel-phone">
	<li class="tel-phone"><a href="#" class="col-lg-auto col-md-auto "><?$APPLICATION->IncludeComponent('bitrix:main.include','',array('AREA_FILE_SHOW'=>'file','PATH'=>SITE_TEMPLATE_PATH.'/include/header/phone2.php'));?></a></li>
</ul>
<?endif?>