<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

        <ul class="inf-menu__list">
			<? foreach ($arResult as $menuList) { ?>
                <li class="inf-menu__item">
                    <a class="inf-menu__link" href="<?= $menuList["LINK"] ?>"><?= $menuList["TEXT"] ?></a>
                </li>
			<? } ?>
        </ul>

<?endif?>
