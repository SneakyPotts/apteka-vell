<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;

if ($normalCount > 0):
?>
<div id="basket_items_list" class="row">
	<div class="col-sm-12 col-md-8 col-lg-9 cart-block">
	<table id="basket_items">
		<thead style="display: none;">
			<tr>
				<?
				foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
					$arHeaders[] = $arHeader["id"];

					// remember which values should be shown not in the separate columns, but inside other columns
					if (in_array($arHeader["id"], array("TYPE")))
					{
						$bPriceType = true;
						continue;
					}
					elseif ($arHeader["id"] == "PROPS")
					{
						$bPropsColumn = true;
						continue;
					}
					elseif ($arHeader["id"] == "DELAY")
					{
						$bDelayColumn = true;
						continue;
					}
					elseif ($arHeader["id"] == "DELETE")
					{
						$bDeleteColumn = true;
						continue;
					}
					elseif ($arHeader["id"] == "WEIGHT")
					{
						$bWeightColumn = true;
					}

					if ($arHeader["id"] == "NAME"):
					?>
						<td class="item" colspan="2" id="col_<?=$arHeader["id"];?>">
					<?
					elseif ($arHeader["id"] == "PRICE"):
					?>
						<td class="price" id="col_<?=$arHeader["id"];?>" style="display: none;">
					<?
					else:
					?>
						<td class="custom custom-price" id="col_<?=$arHeader["id"];?>">
					<?
					endif;
					?>
						<?=$arHeader["name"]; ?>
						</td>
				<?
				endforeach;

				if ($bDeleteColumn || $bDelayColumn):
				?>
					<td class="custom"></td>
				<?
				endif;
				?>
			</tr>
		</thead>

		<tbody>
			<?
			$skipHeaders = array('PROPS', 'DELAY', 'DELETE', 'TYPE');

			foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

				if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
				$countItems++;
				?>
				<tr id="<?=$arItem["ID"]?>"
					 data-item-name="<?=$arItem["NAME"]?>"
					 data-item-brand="<?=$arItem[$arParams['BRAND_PROPERTY']."_VALUE"]?>"
					 data-item-price="<?=$arItem["PRICE"]?>"
					 data-item-currency="<?=$arItem["CURRENCY"]?>"
					 class="basket__item"
				>
					<?
					foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

						if (in_array($arHeader["id"], $skipHeaders)) // some values are not shown in the columns in this template
							continue;

						if ($arHeader["name"] == '')
							$arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);

						if ($arHeader["id"] == "NAME"):
						?>
						
							<td class="basket__img">
								<a class="img_basket" href="<?=$arItem["DETAIL_PAGE_URL"] ?>">
                                    <?$file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width'=>120, 'height'=>120), BX_RESIZE_IMAGE_EXACT, true);?>
                                    <img src="<?=$file["src"]?>">
								</a>
							</td>
							<td class="basket__info">
								<a href="<?=$arItem["DETAIL_PAGE_URL"] ?>">
									<span class="basket__title"><?=$arItem["NAME"]?></span>
								</a>
								<span class="basket__subtitle"><?=$arItem["NAME"]?></span>
                                    <?CModule::IncludeModule('iblock');
                                    $res_p=CIBlockElement::GetList(Array(),Array('IBLOCK_ID'=>4,'ACTIVE_DATE'=>'Y','ACTIVE'=>'Y','ID'=>$arItem['PRODUCT_ID']));
                                    if($res_p->SelectedRowsCount()>0){?>
                                        <?while($ob_p=$res_p->GetNextElement()){$arFields_p=$ob_p->GetFields();$arProps_p=$ob_p->GetProperties();?>
                                            <ul>
                                                <?if(!empty($arProps_p['PROP4']['VALUE'])){?><li>Срок годности: <span><?=$arProps_p['PROP4']['VALUE']?></span></li><?}?>
                                                <?if(!empty($arProps_p['PROP1']['VALUE'])){?><li>Производитель: <span><?=$arProps_p['PROP1']['VALUE']?></span></li><?}?>
                                            </ul>
                                        <?}?>
                                    <?}?>
							</td>
						<?
						elseif ($arHeader["id"] == "QUANTITY"):
						?>
							<td class="custom custom-count">
								<?
								$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
								$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
								$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
								?>
								
								
								<?if(!isset($arItem["MEASURE_RATIO"])){$arItem["MEASURE_RATIO"] = 1;}
								if(floatval($arItem["MEASURE_RATIO"]) != 0){?>
									<div id="basket_quantity_control">
											<a href="javascript:void(0);" class="minus">-</a>
											<input type="text" class="quantity_value" id="QUANTITY_INPUT_<?=$arItem["ID"]?>"  data-id="<?=$arItem['ID']?>" name="QUANTITY_INPUT_<?=$arItem["ID"]?>" maxlength="18" style="max-width: 50px" value="<?=$arItem["QUANTITY"]?>">
											<a href="javascript:void(0);" class="plus" data-quality="<?=$arItem['QUANTITY']?>" data-max="<?=$arItem['AVAILABLE_QUANTITY']?>" >+</a>
									</div>
								<?}?>
								<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>"/>
							</td>
						<?
						elseif ($arHeader["id"] == "PRICE"):
						?>
							<td class="price" style="display: none;">
									<div class="current_price" id="current_price_<?=$arItem["ID"]?>">
										<?=$arItem["PRICE_FORMATED"]?>
									</div>
									<div class="old_price" id="old_price_<?=$arItem["ID"]?>">
										<?if (floatval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0):?>
											<?=$arItem["FULL_PRICE_FORMATED"]?>
										<?endif;?>
									</div>

								<?if ($bPriceType && strlen($arItem["NOTES"]) > 0):?>
									<div class="type_price"><?=GetMessage("SALE_TYPE")?></div>
									<div class="type_price_value"><?=$arItem["NOTES"]?></div>
								<?endif;?>
							</td>
						<?
						elseif ($arHeader["id"] == "DISCOUNT"):
						?>
							<td class="custom">
								<span><?=$arHeader["name"]; ?>:</span>
								<div id="discount_value_<?=$arItem["ID"]?>"><?=$arItem["DISCOUNT_PRICE_PERCENT_FORMATED"]?></div>
							</td>
						<?
						elseif ($arHeader["id"] == "WEIGHT"):
						?>
							<td class="custom">
								<span><?=$arHeader["name"]; ?>:</span>
								<?=$arItem["WEIGHT_FORMATED"]?>
							</td>
						<?
						else:
						?>
							<td class="custom">
								<?/*<span><?=$arHeader["name"]; ?>:</span>*/?>
								<?
								if ($arHeader["id"] == "SUM"):
								?>
									<div id="sum_<?=$arItem["ID"]?>"  class="basket__sum">
								<?
								endif;
if ($arHeader["id"] == "SUM"):
	$arItem[$arHeader["id"]]=str_replace('руб.','₽',$arItem[$arHeader["id"]]);
endif;
								echo $arItem[$arHeader["id"]];

								if ($arHeader["id"] == "SUM"):
								?>
									</div>
								<?
								endif;
								?>
							</td>
						<?
						endif;
					endforeach;

					if ($bDelayColumn || $bDeleteColumn):
					?>
						<td class="control">
							<?
							if ($bDeleteColumn):
								?>
								<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>"
									onclick="return deleteProductRow(this)">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/images/icons/del.png">
								</a>
								<br />
								<?
							endif;
							if ($bDelayColumn):
							?>
								<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delay"])?>"><?=GetMessage("SALE_DELAY")?></a>
							<?
							endif;
							?>
						</td>
					<?
					endif;
					?>
				</tr>
				<?
				endif;
			endforeach;
			?>
		</tbody>
	</table>

	<input type="hidden" id="column_headers" value="<?=htmlspecialcharsbx(implode($arHeaders, ","))?>" />
	<input type="hidden" id="offers_props" value="<?=htmlspecialcharsbx(implode($arParams["OFFERS_PROPS"], ","))?>" />
	<input type="hidden" id="action_var" value="<?=htmlspecialcharsbx($arParams["ACTION_VARIABLE"])?>" />
	<input type="hidden" id="quantity_float" value="<?=($arParams["QUANTITY_FLOAT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />
	</div>
	<div class="col-sm-12 col-md-4 col-lg-3">

		<?/*
		<div class="cart-rightside" id="coupons_block">
		<?
		if ($arParams["HIDE_COUPON"] != "Y")
		{
		?>
			<div class="bx_ordercart_coupon">
				<span><?=GetMessage("STB_COUPON_PROMT")?></span><input type="text" id="coupon" name="COUPON" value="" onchange="enterCoupon();">&nbsp;<a class="bx_bt_button bx_big" href="javascript:void(0)" onclick="enterCoupon();" title="<?=GetMessage('SALE_COUPON_APPLY_TITLE'); ?>"><?=GetMessage('SALE_COUPON_APPLY'); ?></a>
			</div><?
				if (!empty($arResult['COUPON_LIST']))
				{
					foreach ($arResult['COUPON_LIST'] as $oneCoupon)
					{
						$couponClass = 'disabled';
						switch ($oneCoupon['STATUS'])
						{
							case DiscountCouponsManager::STATUS_NOT_FOUND:
							case DiscountCouponsManager::STATUS_FREEZE:
								$couponClass = 'bad';
								break;
							case DiscountCouponsManager::STATUS_APPLYED:
								$couponClass = 'good';
								break;
						}
						?><div class="bx_ordercart_coupon"><input disabled readonly type="text" name="OLD_COUPON[]" value="<?=htmlspecialcharsbx($oneCoupon['COUPON']);?>" class="<? echo $couponClass; ?>"><span class="<? echo $couponClass; ?>" data-coupon="<? echo htmlspecialcharsbx($oneCoupon['COUPON']); ?>"></span><div class="bx_ordercart_coupon_notes"><?
						if (isset($oneCoupon['CHECK_CODE_TEXT']))
						{
							echo (is_array($oneCoupon['CHECK_CODE_TEXT']) ? implode('<br>', $oneCoupon['CHECK_CODE_TEXT']) : $oneCoupon['CHECK_CODE_TEXT']);
						}
						?></div></div><?
					}
					unset($couponClass, $oneCoupon);
				}
		}
		else
		{
			?>&nbsp;<?
		}
?>
		</div>*/?>
		<div class="cart-rightside">
			<div class="cart_right">
				<span class="left_info">В корзине</span>
				<span class="total"><?=$countItems?> товаров</span>
			</div>
			<div class="cart_right">
				<span class="left_info">На сумму</span>
				<span class="total" id="allSum_FORMATED"><span class="itog_summ"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></span></span>
			</div>
			
			<a href="javascript:void(0)" onclick="checkOut();" class="checkout"><?=GetMessage("SALE_ORDER")?></a>
		</div>
	</div>
</div>
<?
else:
?>
<div id="basket_items_list">
	<table>
		<tbody>
			<tr>
				<td style="text-align:center">
					<div class=""><?=GetMessage("SALE_NO_ITEMS");?></div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?endif;?>
<script>
    $(document).ready(function(){
        var $sliderheight = $('.basket__info').height();
        $('.basket__img').height($sliderheight);
    });
</script>
