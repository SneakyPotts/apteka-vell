<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
	<div class="basket__icon">
		<a href="<?=$arParams['PATH_TO_BASKET']?>">
			<img src="<?=SITE_TEMPLATE_PATH?>/assets/images/icons/basket.png" alt="basket">
			<div class="num-wrapper"><span class="numm"><?=$arResult['NUM_PRODUCTS']?></span></div>
		</a>
	</div>
	<a href="<?=$arParams['PATH_TO_BASKET']?>" class="basket__text">
		<div class="basket__title">
			Корзина
		</div>
		<div class="basket__summ">
			<span class="summ"><?=str_replace(' руб.','',$arResult['TOTAL_PRICE'])?> ₽</span>
		</div>
	</a>