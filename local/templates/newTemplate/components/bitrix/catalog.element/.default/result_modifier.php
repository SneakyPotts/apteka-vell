<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

global $APPLICATION;
global $USER;
//if ($USER->IsAdmin()) {
//$mainSection = $this->__component;
//if (is_object($mainSection)) {
//	$mainSection->arResult['MAIN_SECTION'] = $arResult['PROPERTIES']['MAIN_SECTION_REDIRECT']['VALUE'];
//	$mainSection->arResult['ELEMENT_CODE_URL'] =$arResult['ORIGINAL_PARAMETERS']['ELEMENT_CODE'];
//	$mainSection->SetResultCacheKeys(array('MAIN_SECTION','ELEMENT_CODE_URL'));
//	$arResult['MAIN_SECTION'] = $mainSection->arResult['MAIN_SECTION'];
//	$arResult['ELEMENT_CODE_URL'] = $mainSection->arResult['ELEMENT_CODE_URL'];
//}
//
//}
$mainSection = $this->__component;
if (is_object($mainSection)) {
	$mainSection->arResult['MAIN_SECTION'] = $arResult['PROPERTIES']['MAIN_SECTION_REDIRECT']['VALUE'];
	$mainSection->arResult['ELEMENT_CODE_URL'] =$arResult['ORIGINAL_PARAMETERS']['ELEMENT_CODE'];
	$mainSection->SetResultCacheKeys(array('MAIN_SECTION','ELEMENT_CODE_URL'));
	$arResult['MAIN_SECTION'] = $mainSection->arResult['MAIN_SECTION'];
	$arResult['ELEMENT_CODE_URL'] = $mainSection->arResult['ELEMENT_CODE_URL'];
}