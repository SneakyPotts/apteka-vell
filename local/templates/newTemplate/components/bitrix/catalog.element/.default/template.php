<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
$this->setFrameMode(true);
// $this->addExternalCss('/bitrix/css/main/bootstrap.css');

$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
	'ITEM' => array(
		'ID' => $arResult['ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
		'JS_OFFERS' => $arResult['JS_OFFERS']
	)
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId.'_dsc_pict',
	'STICKER_ID' => $mainId.'_sticker',
	'BIG_SLIDER_ID' => $mainId.'_big_slider',
	'BIG_IMG_CONT_ID' => $mainId.'_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId.'_slider_cont',
	'OLD_PRICE_ID' => $mainId.'_old_price',
	'PRICE_ID' => $mainId.'_price',
	'DISCOUNT_PRICE_ID' => $mainId.'_price_discount',
	'PRICE_TOTAL' => $mainId.'_price_total',
	'SLIDER_CONT_OF_ID' => $mainId.'_slider_cont_',
	'QUANTITY_ID' => $mainId.'_quantity',
	'QUANTITY_DOWN_ID' => $mainId.'_quant_down',
	'QUANTITY_UP_ID' => $mainId.'_quant_up',
	'QUANTITY_MEASURE' => $mainId.'_quant_measure',
	'QUANTITY_LIMIT' => $mainId.'_quant_limit',
	'BUY_LINK' => $mainId.'_buy_link',
	'ADD_BASKET_LINK' => $mainId.'_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId.'_not_avail',
	'COMPARE_LINK' => $mainId.'_compare_link',
	'TREE_ID' => $mainId.'_skudiv',
	'DISPLAY_PROP_DIV' => $mainId.'_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId.'_main_sku_prop',
	'OFFER_GROUP' => $mainId.'_set_group_',
	'BASKET_PROP_DIV' => $mainId.'_basket_prop',
	'SUBSCRIBE_LINK' => $mainId.'_subscribe',
	'TABS_ID' => $mainId.'_tabs',
	'TAB_CONTAINERS_ID' => $mainId.'_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId.'_small_card_panel',
	'TABS_PANEL_ID' => $mainId.'_tabs_panel'
);
$obName = $templateData['JS_OBJ'] = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
	: $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);
	$showSliderControls = false;

	foreach ($arResult['OFFERS'] as $offer)
	{
		if ($offer['MORE_PHOTO_COUNT'] > 1)
		{
			$showSliderControls = true;
			break;
		}
	}
}
else
{
	$actualItem = $arResult;
	$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top'
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}
?>

<?CModule::IncludeModule('iblock');
$res_rev1=CIBlockElement::GetList(Array(),Array('IBLOCK_ID'=>7,'ACTIVE_DATE'=>'Y','ACTIVE'=>'Y','PROPERTY_PROD'=>$arResult['ID']));
$revCount = $res_rev1->SelectedRowsCount();
?>
<?
$this->SetViewTarget("title");?>
<h1 class="h1-title"><?= $arResult['NAME']?></h1>
<?$this->EndViewTarget();
/*$APPLICATION->SetPageProperty("title", "Купить ".$arResult['NAME']."в аптеке Москвы");
$APPLICATION->SetPageProperty("description", "В аптеке ВЕЛЛ можно недорого купить ".$arResult['NAME'].", Цена - ".number_format($arResult['PRICES']['BASE']['VALUE'],0,',',' ')."₽. Сделайте заказ, проконсультируйтесь со специалистом, получите качественный лекарственный препарат.");*/

$APPLICATION->SetPageProperty("title", $arResult["PROPERTIES"]["VELL_META_TITLE"]["VALUE"]);
$APPLICATION->SetPageProperty("description", $arResult["PROPERTIES"]["VELL_META_TITLE"]["VALUE"]);

?>
<script>
var mydivs=new Array('#apteka','#instr', '#reviews');
function opcl(arr, e){
    if ($(e).css('display') == 'none'){
        for(var i in arr){  
           $(arr[i]).hide();
        }
        $(e).show(); 
    }
}
</script>
<div class="catalog-product">
	<div class="product-info row">
		<div class="product-img col-md-3">
			<?if(!empty($arResult['DETAIL_PICTURE']['SRC'])){?>
                <?$file = CFile::ResizeImageGet($arResult['DETAIL_PICTURE']["ID"], array('width'=>278, 'height'=>278), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);?>
                <img src="<?=$file['src']?>">
			<?}else{?>
                <?$file = CFile::ResizeImageGet($arResult['PREVIEW_PICTURE']["ID"], array('width'=>278, 'height'=>278), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);?>
                <img src="<?=$file['src']?>">
			<?}?>
		</div>
		<div class="product-info__text col-md-6">
			<ul class="product-info__list">
				<li>Артикул: <span><?=$arResult['PROPERTIES']['ARTICLE']['VALUE']?></span></li>
				<li>Производитель: <span><?=$arResult['PROPERTIES']['PROP1']['VALUE']?></span></li>
				<li>Страна-производитель: <span><?=$arResult['PROPERTIES']['PROP2']['VALUE']?></span></li>
				<li>МНН: <span><?=$arResult['PROPERTIES']['MNN']['VALUE']?></span></li>
				<li>Показания к применению: <span><?foreach($arResult['PROPERTIES']['POKAZ']['VALUE'] as $pokaz):?><?=$pokaz?> <?endforeach;?></span></li>
				<li>Путь введения: <span><?foreach($arResult['PROPERTIES']['VVEDENIE']['VALUE'] as $vved):?><?=$vved?> <?endforeach;?></span></li>
				<li>Срок годности: <span><?=$arResult['PROPERTIES']['PROP4']['VALUE']?></span></li>
			</ul>
		</div>
        <div class="col-md-3 d-lg-none">
            <div class="row">
                <div class="catItem__price col-6 col-md-12">
                    <span><?=number_format($arResult['PRICES']['BASE']['VALUE'],0,',',' ');?> ₽</span>
                    <?if($arResult['CATALOG_QUANTITY']>0){?>
                        <p class="avail">В наличии</p>
                    <?}else{?>
                        <p class="zakaz">На заказ</p>
                    <?}?>
                </div>
                <div class="catItem__buy col-6 col-md-12">
                    <?if($arResult['CATALOG_QUANTITY']>0){?>
                        <a href="?action=ADD2BASKET&id=<?=$arResult['ID']?>" class="add2cart" data-id="<?=$arResult['ID']?>"><img src="<?=SITE_TEMPLATE_PATH?>/assets/images/buy1.png"></a>
                    <?}else{?>

                        <a href="javascript:void(0);" class="askBtn" data-toggle="modal" data-target="#askModal" data-title="<?=$arResult['NAME']?>" data-id="<?=$arResult['ID']?>">
                            <img class="askImg" src="<?=SITE_TEMPLATE_PATH?>/assets/images/icons/ask2.png" alt="<?=$arResult['NAME']?>_buy">
                        </a>
                    <?}?>
                </div>
            </div>
        </div>
		<div class="catItem__price col-md-2 d-none d-lg-block">
			<span><?=number_format($arResult['PRICES']['BASE']['VALUE'],0,',',' ');?> ₽</span>
			<?if($arResult['CATALOG_QUANTITY']>0){?>
			<p class="avail">В наличии</p>
			<?}else{?>
			<p class="zakaz">На заказ</p>
			<?}?>
		</div>
		<div class="catItem__buy col-md-1 d-none d-lg-block">
		<?if($arResult['CATALOG_QUANTITY']>0){?>
			<a href="?action=ADD2BASKET&id=<?=$arResult['ID']?>" class="add2cart" data-id="<?=$arResult['ID']?>"><img src="<?=SITE_TEMPLATE_PATH?>/assets/images/buy1.png"></a>
		<?}else{?>

			<a href="javascript:void(0);" class="askBtn" data-toggle="modal" data-target="#askModal" data-title="<?=$arResult['NAME']?>" data-id="<?=$arResult['ID']?>">
				<img class="askImg" src="<?=SITE_TEMPLATE_PATH?>/assets/images/icons/ask2.png" alt="<?=$arResult['NAME']?>_buy">
			</a>
		<?}?>
		</div>
	</div>
	<div class="product-tabs" id="tabs">
		<ul class="control">
            <?if(!empty($arResult['PROPERTIES']['DETAIL1']['VALUE']['TEXT']) || !empty($arResult['PROPERTIES']['VELL_SEO_TEXT']['VALUE'])){?>
			<li class="active" data-to="apteka" current_looks_param="1" onclick="opcl(mydivs,'#apteka');"><a href="javascript:void(0);">Описание</a></li>
            <?}?>
            <?if(!empty($arResult['DETAIL_TEXT']) || !empty($arResult['PROPERTIES']['DETAIL2']['VALUE']['TEXT'])){?>
            <li <?if(empty($arResult['PROPERTIES']['DETAIL1']['VALUE']['TEXT']) && empty($arResult['PROPERTIES']['VELL_SEO_TEXT']['VALUE'])){?>class="active"<?}?> data-to="instr" current_looks_param="2" onclick="opcl(mydivs,'#instr');"><a href="javascript:void(0);">Инструкция</a></li>
            <?}?>
            <li <?if(empty($arResult['PROPERTIES']['DETAIL1']['VALUE']['TEXT']) && empty($arResult['PROPERTIES']['VELL_SEO_TEXT']['VALUE']) && $arResult['DETAIL_TEXT'] ==NULL && $arResult['PROPERTIES']['DETAIL2']['VALUE']['TEXT'] ==NULL){?>class="active"<?}?>  data-to="rev" current_looks_param="3" onclick="opcl(mydivs,'#reviews');"><a href="javascript:void(0);">Отзывы <!--(<?=$revCount?>)--></a><a class="count-review-catalog"></a></span></li>
		</ul>
	</div>
     <?if(!empty($arResult['PROPERTIES']['DETAIL1']['VALUE']['TEXT']) || !empty($arResult['PROPERTIES']['VELL_SEO_TEXT']['VALUE'])){?>
        <div id="apteka">
            <div class="apteka-text">
                <?=htmlspecialchars_decode($arResult['PROPERTIES']['DETAIL1']['VALUE']['TEXT'])?>
                <?=htmlspecialchars_decode($arResult['PROPERTIES']['VELL_SEO_TEXT']['VALUE']['TEXT'])?>
            </div>
<!--            Показать ещё в карточке товара при маленькой ширине -->
<!--            <a href="javascript:void(0);" id="show-apteka">Показать еще</a>-->
        </div>
    <?}?>
	<div id="instr" <?if(!empty($arResult['PROPERTIES']['DETAIL1']['VALUE']['TEXT']) || !empty($arResult['PROPERTIES']['VELL_SEO_TEXT']['VALUE'])){?>style="display:none;"<?}?>>
        <div class="instr-text">
            <?=htmlspecialchars_decode($arResult['PROPERTIES']['DETAIL2']['VALUE']['TEXT'])?>
            <?=htmlspecialchars_decode($arResult['DETAIL_TEXT'])?>
        </div>
<!--        Показать ещё в описании товара при маленькой ширине -->
<!--        <a href="javascript:void(0);" id="show-instr">Показать еще</a>-->
	</div>
	<div id="reviews" style="display:none;">
					<?if(Bitrix\Main\Loader::includeModule('api.uncachedarea')&& Bitrix\Main\Loader::includeModule('api.reviews')) {
					  CAPIUncachedArea::includeFile("/local/includes/reviews.php",
							 array(
								'ID'   => $arResult['ID'],
								'NAME' => $arResult['NAME'],
						 	)
					  );
					}
					?>
		<?/*<div class="row">
			<div class="reviewsList col-md-6">
				<span>Отзывы (<?=$revCount?>)</span>

<?CModule::IncludeModule('iblock');
$res_rev=CIBlockElement::GetList(Array(),Array('IBLOCK_ID'=>7,'ACTIVE_DATE'=>'Y','ACTIVE'=>'Y','PROPERTY_PROD'=>$arResult['ID']));
if($res_rev->SelectedRowsCount()>0){?>
	<?while($ob_rev=$res_rev->GetNextElement()){$arFields_rev=$ob_rev->GetFields();$arProps_rev=$ob_rev->GetProperties();?>
		<div class="reviewBlock">
			<div class="review_img"><img src="/images/rev.png" alt="review_img"></div>
			<div class="review_title">
				<span class="review_name"><?=$arProps_rev['USERNAME']['VALUE']?></span>
				<span class="review_date"><?=$arFields_rev['DATE_CREATE']?></span>
			</div>
			<div class="review_text">
				<?=htmlspecialchars_decode($arFields_rev['PREVIEW_TEXT'])?>
				<br>
				<br>
				Плюсы: <?=htmlspecialchars_decode($arProps_rev['PLUS']['VALUE']['TEXT'])?>
				<br>
				<br>
				Минусы: <?=htmlspecialchars_decode($arProps_rev['MINUS']['VALUE']['TEXT'])?>
			</div>
		</div>
	<?}?>
<?} else {?>
	<div class="reviewEmpty">
		<span>Отзывов пока нет.</span>
		<p>Станьте первым, кто <a class="addReviewLink" href="javascript:void(0);">оставить</a> отзыв</p>
	</div>
<?}?>
				
			</div>
			<div class="reviewsList col-md-6">
				<button type="button" class="addReview" data-toggle="modal" data-target="#reviewModal">Написать отзыв</button>
			</div>
		</div>
	*/?>
	</div>
</div>

<?$GLOBALS['productID']=$arResult['ID'];?>

<script>
function showReviewModal() {
	// $('.api_modal').addClass('fadeIn');
	// $('.api_modal .api_modal_dialog').addClass('fadeIn');
	$('.api_modal').toggle();
	$('.api_modal').css('opacity','1');
	$('.api_modal .api_modal_dialog').fadeIn(400);
	$('.api_modal .api_modal_dialog').css('opacity','1');
    $('.api_modal .api_modal_dialog').css('margin-top','10%');
}
</script>