<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!doctype html>
<html lang="ru">
<head>
	<? $APPLICATION->ShowHead() ?>
	<?// $APPLICATION->ShowCSS() ?>
	<? //$APPLICATION->ShowHeadStrings() ?>
	<meta charset="<?=LANG_CHARSET;?>">
	<title><?$APPLICATION->ShowTitle()?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<? $APPLICATION->ShowMeta("keywords") ?>
	<? $APPLICATION->ShowMeta("description") ?>
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/css/libs.min.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/css/style.min.css">
    <!-- Скрипты для слайдера компонента "Просмотренные товары" -->
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/slick_slide/slick.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/slick_slide/slick-theme.css">
</head>
<body>
<?$APPLICATION->ShowPanel();?>
    <header class="header ">
        <div class="container">




            <div class="row justify-content-end header__contact align-items-end">
                <div class="col-lg-auto schedule tel"><?$APPLICATION->IncludeComponent('bitrix:main.include','',array('AREA_FILE_SHOW'=>'file','PATH'=>SITE_TEMPLATE_PATH.'/include/header/worktime.php'));?></div>
                <?$APPLICATION->IncludeComponent('bitrix:main.include','',array('AREA_FILE_SHOW'=>'file','PATH'=>SITE_TEMPLATE_PATH.'/include/header/phone1.php'));?>
                <?$APPLICATION->IncludeComponent('bitrix:main.include','',array('AREA_FILE_SHOW'=>'file','PATH'=>SITE_TEMPLATE_PATH.'/include/header/phone2.php'));?>
                <a href="javascript:void(0);" class="col-lg-auto col-md-auto align-self-center link" data-toggle="modal" data-target="#callbackModal">ОБРАТНЫЙ ЗВОНОК</a>
            </div>
            <div class="row header__menu justify-content-end align-items-center">
                <a href="/" class="logo col-md-auto">
                    <?$APPLICATION->IncludeComponent('bitrix:main.include','',array('AREA_FILE_SHOW'=>'file','PATH'=>SITE_TEMPLATE_PATH.'/include/header/logo.php'));?>
                </a>
				<!-- Добавлен span контейнер на кнопку бургера и класс для eventa клика открытия меню -->
				<span class="header__menu-btn-container" style="display: flex;width: 40px;height: 40px;justify-content: center;align-items: center;">
                <div class="header__menu-btn">
                    <span></span>
                </div>
				</span>
                <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"COMPONENT_TEMPLATE" => "top",
		"DELAY" => "N",
		"MAX_LEVEL" => "2",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N"
	),
	false
);?>
            </div>
        </div>
    </header>



    <div class="top-block">



        <div class="container">


            <div class="row">



                <div class=" catalog">

                    <div class="catalog__link">КАТАЛОГ</div>
                    <div class="catalog__drop-down">
                        <ul>
							<?CModule::IncludeModule("iblock");
                            $arFilter = Array('IBLOCK_ID'=>4, 'GLOBAL_ACTIVE'=>'Y');
                            $db_list = CIBlockSection::GetList(Array("name" => "asc", "left_margin"=>"asc"), $arFilter, true);
                            while($ar_result = $db_list->GetNext()){
                                if ($ar_result["ELEMENT_CNT"] > 0) {
                                    {?>
                                        <li>
                                            <a href="<?= $ar_result['SECTION_PAGE_URL'] ?>"><?= $ar_result['NAME'] ?></a>
                                        </li>
                                    <?}
                                }
                            }?>
                        </ul>
                    </div>
                </div>



                <div class="col-xl-auto col-xl-12 search">
                    <?$APPLICATION->IncludeComponent(
	"bitrix:search.title", 
	"topsearch", 
	array(
		"NUM_CATEGORIES" => "1",
		"TOP_COUNT" => "5",
		"CHECK_DATES" => "Y",
		"SHOW_OTHERS" => "N",
		"PAGE" => "/search/index.php",
		"SHOW_INPUT" => "Y",
		"INPUT_ID" => "title-search-input",
		"CONTAINER_ID" => "title-search",
		"COMPONENT_TEMPLATE" => "topsearch",
		"ORDER" => "date",
		"USE_LANGUAGE_GUESS" => "Y",
		"CATEGORY_0_TITLE" => "",
		"CATEGORY_0" => array(
			0 => "iblock_catalog",
		),
		"CATEGORY_0_iblock_catalog" => array(
			0 => "4",
		),
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SHOW_PREVIEW" => "Y",
		"CONVERT_CURRENCY" => "N",
		"PREVIEW_WIDTH" => "75",
		"PREVIEW_HEIGHT" => "75"
	),
	false
);?>
                </div>

				<!-- Здесь баннер Новогодний, 8 марта и так далее -->



                    <?$APPLICATION->IncludeComponent(
                    "bitrix:sale.basket.basket.line",
                    ".default",
                    array(
                        "HIDE_ON_BASKET_PAGES" => "N",
                        "PATH_TO_AUTHORIZE" => "",
                        "PATH_TO_BASKET" => "/cart/",
                        "PATH_TO_ORDER" => "/order/",
                        "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                        "PATH_TO_PROFILE" => SITE_DIR."personal/",
                        "PATH_TO_REGISTER" => SITE_DIR."login/",
                        "POSITION_FIXED" => "N",
                        "SHOW_AUTHOR" => "N",
                        "SHOW_EMPTY_VALUES" => "Y",
                        "SHOW_NUM_PRODUCTS" => "Y",
                        "SHOW_PERSONAL_LINK" => "N",
                        "SHOW_PRODUCTS" => "N",
                        "SHOW_REGISTRATION" => "N",
                        "SHOW_TOTAL_PRICE" => "Y",
                        "COMPONENT_TEMPLATE" => ".default"
                    ),
                    false
                );?>
            </div>

        </div>





        <?if(IS_MAIN!='Y'){?>
            <div class="container breadcrumb">
                <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb", 
	"universal", 
	array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "s2",
		"COMPONENT_TEMPLATE" => "universal"
	),
	false
);?>


            </div>
    <?}?>




    </div>



<?if(IS_MAIN!='Y'){?>
<main><div class="container">





    <?if(IS_CATALOG=='Y'){?>
        <div class="catalog__wrap catalog__wrap-title">



            <?$APPLICATION->ShowViewContent("title");?>
            <h1 class="h1-filter 1"><?$APPLICATION->ShowViewContent("sotbit_seometa_h1")?></h1>
        </div>
    <?}?>
<?}?>
<?if(IS_MAIN!='Y'&&IS_CATALOG!='Y'){?>
	<div class="text-wrapper">
<?}?>