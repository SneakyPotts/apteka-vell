ВитФарма – интернет-магазин лекарственных препаратов в Москве –
                        предоставляет широкий ассортимент лекарственных средств, товаров для
                        реабилитации, биологически активных добавок, лечебной косметики, средств
                        личной гигиены, а также товаров для мам и их малышей. Бронирование
                        медикаментов через интернет сэкономит для вас время и деньги, поскольку Вы
                        сразу можете ознакомиться с инструкцией и стоимостью товара, а также
                        убедиться в его наличии. А уже укомплектованный заказ вы получите в нашей
                        аптеке. Также мы предоставляем услуги доставки товаров для льготных
                        категорий граждан. Для этого нужно всего лишь открыть ссылку Доставка на
                        нашем сайте и указать Ваш адрес.