<div class="advantages__title text-center">
	 Преимущества нашей онлайн-аптеки "ВЕЛЛ" <br>
 <br>
</div>
<h4 class="advantages__subtitle">Большой ассортимент противоопухолевых препаратов</h4>
 <br>
<div class="advantages__items row">
	<div class="advantages__item col">
 <img alt="значок иконка качества" src="/local/templates/newTemplate/assets/images/advantages-item-1.png">
		<div class="text">
			 Гарантия качества, только оригинальные лекарства
		</div>
	</div>
	<div class="advantages__item col">
 <img alt="значок кошелька" src="/local/templates/newTemplate/assets/images/advantages-item-2.png">
		<div class="text">
			Все возможные способы оплаты
		</div>
	</div>
	<div class="w-100">
	</div>
	<div class="advantages__item item-3 col">
 <img alt="круглосуточная иконка" src="/local/templates/newTemplate/assets/images/advantages-item-3.png">
		<div class="text">
			 Круглосуточная работа интернет-сайта, делайте заказы 24/7
		</div>
	</div>
	<div class="advantages__item item-3 col">
 <img alt="Оптовые цены на некоторые группы товаров" src="/local/templates/newTemplate/assets/images/advantages-item-4.png">
		<div class="text">
			 Приятные цены, выгодные условия покупки<br>
		</div>
	</div>
</div>
 <br>