<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?$APPLICATION->ShowTitle()?></title>  <?$APPLICATION->ShowHead();?>

  <link rel="stylesheet" href="/local/templates/index/styles.css">
  <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700&amp;subset=cyrillic" rel="stylesheet">
  <link rel="stylesheet" href="/local/templates/index/owl-carousel/owl.carousel.css">
  <link rel="stylesheet" href="/local/templates/index/owl-carousel/owl.theme.css">
  
  

    <link rel="stylesheet" href="/local/templates/index/media.css">



  <script src="/local/templates/index/owl-carousel/owl.carousel.js"></script>
  <script src="/local/templates/index/js/site.js"></script>
  <link rel="icon" href="http://www.vt-farm.ru/favicon.ico">
  <link rel="shortcut icon" href="/favicon_1.ico" type="image/x-icon">
</head>
<body>
  <div id="panel"><?$APPLICATION->ShowPanel();?></div>
  <div class="top">
    <div class="center">
      <div class="topmenu">             
        <?$APPLICATION->IncludeComponent("bitrix:menu", "menu", array(
          "ROOT_MENU_TYPE" => "top2",
          "MENU_CACHE_TYPE" => "Y",
          "MENU_CACHE_TIME" => "36000000",
          "MENU_CACHE_USE_GROUPS" => "Y",
          "MENU_CACHE_GET_VARS" => array(
            ),
          "MAX_LEVEL" => "1",
          "CHILD_MENU_TYPE" => "submenu",
          "USE_EXT" => "N",
          "DELAY" => "N",
          "ALLOW_MULTI_SELECT" => "N"
          ),
        false
        );?>
      </div>
      <
    </div>
  </div>
 <div class="header">
    <div class="center">
		<div class="newflexheader">
			<div class="firsthalf">
				<a href="/" class="header-logo">
					<img src="/local/templates/index/images/logo.png">
				</a>
				<div class="contacts">
					<span class="mail">E-mail: <a href="mailto:apteka.vell@mail.ru">apteka.vell@mail.ru</a></span><br>
					г. Москва, ул. маршала Василевского, 13 корпус 1 (метро Щукинская)
				</div>
			</div>
			<div class="secondhalf">
				<div class="time">
				   Прием заказов<br>
				   Пн - Вс: с 9:00 до 21:00
				</div>
				<div class="phone">
				  +7 (495) 003-13-03<br>
				  +7 (499) 193-56-36<br>
				  <a id="call-back" class="open" onclick="yaCounter23372263.reachGoal ('call');"><span>Обратный звонок</span></a>
				</div>
			</div>
		</div>
    <!-- <div class="header-basket">
      <a class="basket" href="/personal/cart/">
        Корзина
        <span class="count">6</span>
      </a>
    </div> -->
  </div>
</div>
<div class="topmenu2">
  <div class="center">
    <button class="clickssss"></button>               
   <?$APPLICATION->IncludeComponent("bitrix:menu", "menu", array(
    "ROOT_MENU_TYPE" => "top",
    "MENU_CACHE_TYPE" => "Y",
    "MENU_CACHE_TIME" => "36000000",
    "MENU_CACHE_USE_GROUPS" => "Y",
    "MENU_CACHE_GET_VARS" => array(
      ),
    "MAX_LEVEL" => "1",
    "CHILD_MENU_TYPE" => "submenu",
    "USE_EXT" => "N",
    "DELAY" => "N",
    "ALLOW_MULTI_SELECT" => "N"
    ),
   false
   );?>

 </div>
</div>
<div class="bg-search">
  <div class="search">
    <?$APPLICATION->IncludeComponent(
      "bitrix:search.title", 
      "catalog1", 
      array(
        "CATEGORY_0" => array(
          0 => "iblock_catalog",
          ),
        "CATEGORY_0_TITLE" => "",
        "CATEGORY_0_iblock_catalog" => array(
          0 => "5",
          ),
        "CHECK_DATES" => "N",
        "CONTAINER_ID" => "title-search",
        "INPUT_ID" => "title-search-input",
        "NUM_CATEGORIES" => "1",
        "ORDER" => "date",
        "PAGE" => "#SITE_DIR#search/index.php",
        "SHOW_INPUT" => "Y",
        "SHOW_OTHERS" => "N",
        "TOP_COUNT" => "5",
        "USE_LANGUAGE_GUESS" => "Y",
        "COMPONENT_TEMPLATE" => "catalog1",
        "PRICE_CODE" => array(
          0 => "BASE",
          ),
        "PRICE_VAT_INCLUDE" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "",
        "SHOW_PREVIEW" => "Y",
        "CONVERT_CURRENCY" => "N",
        "PREVIEW_WIDTH" => "75",
        "PREVIEW_HEIGHT" => "75"
        ),
      false
      );?>

    </div>
  </div>




  <div class="center">





    <div class="content-index">

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = '2t62lyGd4a';var d=document;var w=window;function l(){var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
</script>
<!-- {/literal} END JIVOSITE CODE -->



<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb", 
	"site", 
	array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "s1",
		"COMPONENT_TEMPLATE" => "site"
	),
	false
);?>