<div class="index-block-1">
    <a class="index-block-1-item">
      <img src="/local/templates/index/images/ic-13.png">
      <span>ТОЛЬКО ПОДЛИННЫЕ И КАЧЕСТВЕННЫЕ ТОВАРЫ</span>
    </a>
    <a class="index-block-1-item">
      <img src="/local/templates/index/images/ic-17.png">
      <span>СОБЛЮДЕННЫЕ СРОК И 
УСЛОВИЯ ХРАНЕНИЯ</span>
    </a>
    <a class="index-block-1-item">
      <img src="/local/templates/index/images/ic-11.png">
      <span>НИЗКИЕ РОЗНИЧНЫЕ ЦЕНЫ, 
КАК У ПРОИЗВОДИТЕЛЯ</span>
    </a>
    <a class="index-block-1-item">
      <img src="/local/templates/index/images/ic-18.png">
      <span>ОПТОВЫЕ ЦЕНЫ НА НЕКОТОРЫЕ 
ГРУППЫ ТОВАРОВ</span>
    </a>
  </div>
  </div>
  </div>
</div>
<div class="footer">
  <div class="center">
    <div class="footer-left">
      <b class="footer-title">Контактная информация</b>
     
      <div class="footer-left-contscts">


<div itemscope itemtype="http://schema.org/LocalBusiness">
		  <b>E-mail:</b> <span itemprop="email">apteka.vell@mail.ru</span><br>
		  <b>Тел.:</b> <span itemprop="telephone">+7 (495) 003-13-03</span><br>
        <b>Моб.:</b> <span itemprop="telephone">+7 (499) 193-56-36</span><br>
<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
		  <p style="text-align: left;"><b>Адрес:</b> г.<span itemprop="addressLocality">Москва</span>, <span itemprop="streetAddress">ул.Маршала Василевского, 13к1</span></p>
	</div>

<div itemprop="image" content="https://apteka-vell.ru/local/templates/index/images/logo.png"></div>
		  <b>Прием заказов:</b><br>




<time
 itemprop="openingHours"
 datetime="Mo-Su 9:00?21:00">

       Пн - Вс: с 9:00 до 21:00<br><br>
	


        <div class="clear"></div>
      <p>Аптека <span itemprop="name">VELL.RU</span></p>



      </div>


      </div>
    </div>
    <div class="footer-right">
      
	  
	  <!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47622841 = new Ya.Metrika({
                    id:47622841,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47622841" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
	  
	  
      <div class="footer-right-menu">
        <b class="footer-title">О компании</b>
        <ul>
          <li><a href="https://apteka-vell.ru/lekarstvennye-sredstva/onkologiya/">Лекарственные средства</a></li>
          <li><a href="https://apteka-vell.ru/about/">О компании</a></li>
          <li><a href="https://apteka-vell.ru/news/">Новости</a></li>
          <li><a href="https://apteka-vell.ru/delivery/">Оплата и доставка</a></li>
          <li><a href="https://apteka-vell.ru/kak-sdelat-zakaz/">Как сделать заказ?</a></li>
          <li><a href="https://apteka-vell.ru/contacts/">Контакты</a></li>
        </ul>


<p style="margin-top: 10px;"><b class="footer-title">Юридическая информация</b></p>

<ul>
          <li><b>ИНН</b> - 7119403499</li>
	<li><b>ОКПО</b> - 40379096</li>
<li><a href="https://apteka-vell.ru/litsenziya/">Лицензия</a></li>

        </ul>


      </div>
      

    </div>
  </div>
</div>

<div id="callback-form" class="popup-form">
        <span class="close">X</span>
        <h2>Заказать обратный звонок</h2>
        <?$APPLICATION->IncludeComponent("bitrix:form.result.new","callback",Array("WEB_FORM_ID" => 2, "LIST_URL" => ""));?>
      </div>
      <?if (($_GET['formresult']=='addok') && ($_GET['WEB_FORM_ID']=='2')):?> 
      <script>history.pushState(null, null, window.location.href.replace("&formresult=addok", ""));</script>
      <div class="popup-form popup_success">
      <span class="close">X</span>
      <h2>спасибо, ваша заявка принята</h2>

      <p>Ваша заявка отправлена, в ближайшее время<br>
      наши менеджеры свяжутся с Вами.</p>
      <p>БЛАГОДАРИМ ВАС ЗА ОБРАЩЕНИЕ!</p>
      </div>
      <?endif?>
</body>
</html>