<?$APPLICATION->AddHeadScript('https://code.jquery.com/jquery-2.2.4.js');?>
<?$APPLICATION->IncludeComponent(
	"api:reviews",
	"reviews",
	Array(
		"CACHE_TIME" => "31536000",
		"CACHE_TYPE" => "A",
		"COLOR" => "green2",
		"DETAIL_HASH" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_ID" => $ID,
		"EMAIL_TO" => "",
		"FORM_CITY_VIEW" => "Y",
		"FORM_DELIVERY" => array(),
		"FORM_DISPLAY_FIELDS" => array("RATING","TITLE","ADVANTAGE","DISADVANTAGE","ANNOTATION","GUEST_NAME","GUEST_EMAIL"),
		"FORM_FORM_SUBTITLE" => "",
		"FORM_FORM_TITLE" => "Ваш отзыв",
		"FORM_MESS_ADD_REVIEW_ERROR" => "Внимание!<br>Ошибка добавления отзыва",
		"FORM_MESS_ADD_REVIEW_EVENT_TEXT" => "<p>#USER_NAME# добавил(а) новый отзыв (оценка: #RATING#) ##ID#</p>
<p>Открыть в админке #LINK_ADMIN#</p>
<p>Открыть на сайте #LINK#</p>",
		"FORM_MESS_ADD_REVIEW_EVENT_THEME" => "Отзыв о магазине VITFARMA.RU (оценка: #RATING#) ##ID#",
		"FORM_MESS_ADD_REVIEW_MODERATION" => "Спасибо!<br>Ваш отзыв отправлен на модерацию",
		"FORM_MESS_ADD_REVIEW_VIZIBLE" => "Спасибо!<br>Ваш отзыв №#ID# опубликован",
		"FORM_MESS_EULA" => "Нажимая кнопку «Отправить отзыв», я принимаю условия Пользовательского соглашения и даю своё согласие на обработку моих персональных данных, в соответствии с Федеральным законом от 27.07.2006 года №152-ФЗ «О персональных данных», на условиях и для целей, определенных Политикой конфиденциальности.",
		"FORM_MESS_EULA_CONFIRM" => "Для продолжения вы должны принять условия Пользовательского соглашения",
		"FORM_MESS_PRIVACY" => "Я согласен на обработку персональных данных",
		"FORM_MESS_PRIVACY_CONFIRM" => "Для продолжения вы должны принять соглашение на обработку персональных данных",
		"FORM_MESS_PRIVACY_LINK" => "",
		"FORM_MESS_STAR_RATING_1" => "Ужасно",
		"FORM_MESS_STAR_RATING_2" => "Плохо",
		"FORM_MESS_STAR_RATING_3" => "Нормально",
		"FORM_MESS_STAR_RATING_4" => "Хорошо",
		"FORM_MESS_STAR_RATING_5" => "Отлично",
		"FORM_PREMODERATION" => "Y",
		"FORM_REQUIRED_FIELDS" => array("RATING","ANNOTATION"),
		"FORM_RULES_LINK" => "",
		"FORM_RULES_TEXT" => "",
		"FORM_SHOP_BTN_TEXT" => "Написать!",
		"FORM_SHOP_TEXT" => "Ваш отзыв",
		"FORM_USE_EULA" => "Y",
		"FORM_USE_PRIVACY" => "Y",
		"IBLOCK_ID" => "",
		"INCLUDE_CSS" => "Y",
		"INCLUDE_JQUERY" => "N",
		"LIST_ACTIVE_DATE_FORMAT" => "",
		"LIST_ITEMS_LIMIT" => "10",
		"LIST_MESS_ADD_UNSWER_EVENT_TEXT" => "#USER_NAME#, здравствуйте! 
К Вашему отзыву добавлен официальный ответ, для просмотра перейдите по ссылке #LINK#",
		"LIST_MESS_ADD_UNSWER_EVENT_THEME" => "Официальный ответ к вашему отзыву",
		"LIST_MESS_HELPFUL_REVIEW" => "Отзыв полезен?",
		"LIST_MESS_TRUE_BUYER" => "Проверенный покупатель",
		"LIST_SET_TITLE" => "N",
		"LIST_SHOP_NAME_REPLY" => "Аптека Витфарма",
		"LIST_SHOW_THUMBS" => "Y",
		"LIST_SORT_FIELDS" => array("ACTIVE","ACTIVE_FROM","RATING","THUMBS"),
		"LIST_SORT_FIELD_1" => "ACTIVE_FROM",
		"LIST_SORT_FIELD_2" => "DATE_CREATE",
		"LIST_SORT_FIELD_3" => "ID",
		"LIST_SORT_ORDER_1" => "DESC",
		"LIST_SORT_ORDER_2" => "DESC",
		"LIST_SORT_ORDER_3" => "DESC",
		"MESSAGE_404" => "",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "31536000",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_THEME" => "blue",
		"PAGER_TITLE" => "Отзывы",
		"PICTURE" => array(),
		"RESIZE_PICTURE" => "48x48",
		"SECTION_ID" => "",
		"SEF_MODE" => "N",
		"SET_STATUS_404" => "N",
		"SHOP_NAME" => "Аптека Vell",
		"SHOW_404" => "N",
		"STAT_MESS_CUSTOMER_RATING" => "На основе #N# оценок покупателей",
		"STAT_MESS_CUSTOMER_REVIEWS" => "Отзывы реальных покупателей <span class=\"api-reviews-count\"></span>",
		"STAT_MESS_TOTAL_RATING" => "Рейтинг покупателей",
		"STAT_MIN_AVERAGE_RATING" => "5",
		"THEME" => "flat",
		"THUMBNAIL_HEIGHT" => "72",
		"THUMBNAIL_WIDTH" => "114",
		"UPLOAD_FILE_LIMIT" => "8",
		"UPLOAD_FILE_SIZE" => "10M",
		"UPLOAD_FILE_TYPE" => "",
		"UPLOAD_VIDEO_LIMIT" => "8",
		"URL" => "",
		"USE_FORM_MESS_FIELD_PLACEHOLDER" => "N",
		"USE_MESS_FIELD_NAME" => "N",
		"USE_STAT" => "Y",
		"USE_SUBSCRIBE" => "N",
		"USE_USER" => "N",
		"VARIABLE_ALIASES" => Array("review_id"=>"review_id","user_id"=>"user_id")
	)
);?>