<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

/*
* config
*/
$config['IBLOCK_ID'] = 7;
$config['EMAIL_TEMPLATE'] = "new-review";
/*
*/
$errors = array();
$name = trim($_POST['username']);
$rate = trim($_POST['rate']);
$plus = trim($_POST['plus']);
$minus = trim($_POST['minus']);
$prod = trim($_POST['prod']);
$comment = trim($_POST['comment']);

if(empty($errors)) {
    $el = new CIBlockElement;
    $PROP = array();
    $PROP['USERNAME'] = $name;
    $PROP['RATE'] = $rate;
    $PROP['MINUS'] = $minus;
    $PROP['PLUS'] = $plus;
    $PROP['PROD'] = $prod;

    $arLoadProductArray = Array(
        "MODIFIED_BY"     => $USER->GetID(),
        "IBLOCK_ID"         => $config['IBLOCK_ID'],
        "PROPERTY_VALUES"=> $PROP,
        "NAME"             => $name,
        "ACTIVE"         => "Y",
        "PREVIEW_TEXT" => $comment,
    );
    if($PRODUCT_ID = $el->Add($arLoadProductArray)) {
        // echo "New ID: ".$PRODUCT_ID;
    } else {
        echo "Error: ".$el->LAST_ERROR;
    }

    $EVENT_TYPE = $config['EMAIL_TEMPLATE'];
    CEvent::Send($EVENT_TYPE, SITE_ID, $PROP);

    $res = array();
    $res['success'] = true;
    echo json_encode($res);
} else {
    $res = array();
    $res['success'] = false;
    $res['errors'] = $errors;
    echo json_encode($res);
}
?>