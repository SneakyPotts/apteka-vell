<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");

$type=1;
global $USER;


$useremail=trim($_REQUEST['useremail']);
$fio=trim($_REQUEST['username']);$fioFull=explode(' ',$fio);
$userphone=trim($_REQUEST['userphone']);
$comments=trim($_REQUEST['comments']);

$delivery=3;
$payment=1;


if(empty($errors)) {
    
// guest
if (!$USER->IsAuthorized()) {

    // register new user
    $NEW_LOGIN = $useremail;
    $NEW_EMAIL = $useremail;
    $NEW_NAME = $fioFull[0];
    $NEW_LAST_NAME = $fioFull[1];
    
    $dbUserLogin = CUser::GetByLogin($NEW_LOGIN);
    
    if ($arUserLogin = $dbUserLogin->Fetch()) {
        $USER->Authorize($arUserLogin['ID']);
        $newLoginTmp = $NEW_LOGIN;
        $uind = 0;
        do
        {
            $uind++;
            if ($uind == 10)
            {
                $NEW_LOGIN = $useremail;
                $newLoginTmp = $NEW_LOGIN;
            }
            elseif ($uind > 10)
            {
                $NEW_LOGIN = "buyer".time().GetRandomCode(2);
                $newLoginTmp = $NEW_LOGIN;
                break;
            }
            else
            {
                $newLoginTmp = $NEW_LOGIN.$uind;
            }
            $dbUserLogin = CUser::GetByLogin($newLoginTmp);
        }
        while ($arUserLogin = $dbUserLogin->Fetch());
        $NEW_LOGIN = $newLoginTmp;
    }
    
    $password_chars = array(
        "abcdefghijklnmopqrstuvwxyz",
        "ABCDEFGHIJKLNMOPQRSTUVWXYZ",
        "0123456789",
    );
    $NEW_PASSWORD = $NEW_PASSWORD_CONFIRM = randString(8, $password_chars);
    $user = new CUser;
    $arAuthResult = $user->Add(Array(
        "LOGIN" => $NEW_EMAIL,
        "NAME" => $NEW_NAME,
        "LAST_NAME" => $NEW_LAST_NAME,
        "PASSWORD" => $NEW_PASSWORD,
        "CONFIRM_PASSWORD" => $NEW_PASSWORD_CONFIRM,
        "EMAIL" => $NEW_EMAIL,
        "GROUP_ID" => 5,
        "ACTIVE" => "Y",
        "LID" => "s1",
        )
    );
    $arFields["USER_ID"] = $arAuthResult;
    $USER->Authorize($arAuthResult);
    if ($USER->IsAuthorized()) {
        CUser::SendUserInfo($USER->GetID(), "s1", "регистрация", true);
    } else    {
        $arResult["ERROR_MESSAGE"]  .= GetMessage("STOF_ERROR_REG_CONFIRM");
        //echo "auth".$arResult["ERROR_MESSAGE"];
    }
}    
    
    
    
    
    $arFields = array(
        "LID" => "s1",
        "PERSON_TYPE_ID" => $type,
        "PAYED" => "N",
        "CANCELED" => "N",
        "STATUS_ID" => "N",
        "PRICE" => 1000,
        "CURRENCY" =>"RUB",
        "USER_ID" => IntVal($USER->GetID()),
        "PAY_SYSTEM_ID" => $payment,
        "PRICE_DELIVERY" => 0,
        "DELIVERY_ID" => $delivery,
        "DISCOUNT_VALUE" => 0,
        "TAX_VALUE" => "",
        "USER_DESCRIPTION" => $comments
    );
    
    $arResult["ORDER_ID"] = CSaleOrder::Add($arFields);
    $arResult["ORDER_ID"] = IntVal($arResult["ORDER_ID"]);
    
    
    
        // email
        $arFields1 = array("ORDER_ID" => $arResult["ORDER_ID"],"ORDER_PROPS_ID" => 1, "NAME" =>"Ф.И.О.", "CODE"=>"FIO" , "VALUE" => $fio);
        CSaleOrderPropsValue::Add($arFields1);
        // fio
        $arFields1 = array("ORDER_ID" => $arResult["ORDER_ID"],"ORDER_PROPS_ID" => 2,"NAME" =>"E-Mail","CODE" => "EMAIL","VALUE" => $useremail);
        CSaleOrderPropsValue::Add($arFields1);
        // phone
        $arFields1 = array("ORDER_ID" => $arResult["ORDER_ID"],"ORDER_PROPS_ID" => 3,"NAME" =>"Телефон","CODE" => "PHONE","VALUE" => $userphone);
        CSaleOrderPropsValue::Add($arFields1);
        
        
    
    
    CSaleBasket::OrderBasket($arResult["ORDER_ID"]);


    
$USER->Authorize($arAuthResult);

    $res = array();
    $res['success'] = true;
    echo json_encode($res);
    
} else {
    
    $res = array();
    $res['success'] = false;
    $res['errors'] = $errors;
    echo json_encode($res);

}

?>