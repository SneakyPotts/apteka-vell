<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

/*
* config
*/
$config['IBLOCK_ID'] = 8;
$config['EMAIL_TEMPLATE'] = "new-callback";
/*
*/
$errors = array();
$name = trim($_POST['username']);
$userphone = trim($_POST['userphone']);
$comment = trim($_POST['comment']);

if(empty($errors)) {
    $el = new CIBlockElement;
    $PROP = array();
    $PROP['USERNAME'] = $name;
    $PROP['USERPHONE'] = $userphone;

    $arLoadProductArray = Array(
        "MODIFIED_BY"     => $USER->GetID(),
        "IBLOCK_ID"         => $config['IBLOCK_ID'],
        "PROPERTY_VALUES"=> $PROP,
        "NAME"             => $name,
        "ACTIVE"         => "Y",
        "PREVIEW_TEXT" => $comment,
    );
    if($PRODUCT_ID = $el->Add($arLoadProductArray)) {
        // echo "New ID: ".$PRODUCT_ID;
    } else {
        echo "Error: ".$el->LAST_ERROR;
    }

    $EVENT_TYPE = $config['EMAIL_TEMPLATE'];
    CEvent::Send($EVENT_TYPE, SITE_ID, $PROP);

    $res = array();
    $res['success'] = true;
    echo json_encode($res);
} else {
    $res = array();
    $res['success'] = false;
    $res['errors'] = $errors;
    echo json_encode($res);
}
?>