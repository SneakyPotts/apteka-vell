<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как сделать заказ");
?><h2>
Как сделать заказ<br>
 </h2>
<p>
	 1. Выбор товара<br>
	 Выбрать нужный товар можно из левого меню, перейдя в нужный каталог. Или же воспользоваться главной страницей сайта.
</p>
<h3>
2. Добавление товара в корзину</h3>
<p>
	 Как только Вы определились с товаром, нажмите на кнопку «В корзину». Товар будет автоматически добавлен в Вашу корзину. Количество добавленных товаров отображается в поле «Корзина», которое находится в шапке сайта.
</p>
<p>
	 Далее, если Вы закончили выбирать товар, нажмите поле «Корзина» и Вы попадете на страницу «Моя корзина». На этой странице будут перечислены все выбранные Вами товары. В поле «Количество» Вы можете изменить количество товара для покупки. После изменения количества товара сумма будет пересчитана автоматически. Также Вы можете удалить ненужный товар.
</p>
<h3>
3. Оформление и подтверждение заказа</h3>
<p>
	 После того как Вы окончательно сформировали свой заказ нажмите на кнопку «Оформить заказ».
</p>
<p>
	 Введите всю необходимую: ФИО получателя, контактные данные. Поля, помеченные звездочками, обязательны для заполнения. Далее, для завершения процедуры оформления заказа Вам нужно нажать кнопку «Оформить заказ».
</p>
<p>
	 Внимание! Неправильно указанный номер телефона может привести к дополнительной задержке! Пожалуйста, внимательно проверяйте Ваши персональные данные при оформлении заказа.&nbsp;
</p>
<h3>
4. Работа с заказом</h3>
<div>
	Заказы обрабатываются в рабочие дни с 9.00 до 21.00.
</div>
<p>
	 После того как Вы заказали товар в нашей фирме, на Ваш e-mail адрес будет выслано письмо с № заказа — подтверждение того, что заказ принят. Затем с Вами свяжется наш консультант, уточнит детали заказа и ответит на все интересующие Вас вопросы. Заказанный Вами товар будет зарезервирован на складе.
</p>
<p>
	 Если консультанту в течение двух рабочих дней не удается связаться с покупателем и подтвердить заказ, на e-mail адрес будет выслано письмо с предупреждением, что заказ может быть аннулирован по этой причине.
</p>
<p>
	 После того как наш консультант подтвердит резерв Вашего заказа, Вы можете оплатить его, если не сделали этого ранее. Как только мы получим подтверждение Вашей оплаты, заказ будет собран.<br>
</p>
<p>
	 Вы можете приступить к выбору товара, перейдя в Категории товаров.
</p>
<p>
	 Больше информации, вы можете узнать, позвонив по телефону&nbsp;+7 (495) 003-13-03 или +7 (499) 193-56-36
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>